package au.com.alphsigma.intouch;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Environment;
import android.telephony.TelephonyManager;

public class Utils {
	Context context;
	public static  int[] thumbs = new int[]{
			R.drawable.thumb_one,
			R.drawable.thumb_two,
			R.drawable.thumb_three,
			R.drawable.thumb_four, 
			R.drawable.thumb_five,
			R.drawable.thumb_six,
			R.drawable.thumb_seven,
			R.drawable.thumb_eight,
			R.drawable.thumb_nine,
			R.drawable.thumb_11,
			R.drawable.thumb_13,
			R.drawable.thumb_14,
			R.drawable.thumb_15,
			R.drawable.thumb_16
	//		R.drawable.thumb_ten
	};
	
	
	public static String getUID(Context context) {
		String UID = null;
		if (context != null) {
			TelephonyManager telMgr =  (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
			String deviceID = telMgr.getDeviceId();
			String simNumber = telMgr.getSimSerialNumber();
			String phNumber = telMgr.getLine1Number();
		    String androidId = android.provider.Settings.Secure.getString(context.getContentResolver(), 
				   								android.provider.Settings.Secure.ANDROID_ID);

		    UID = deviceID +simNumber+phNumber+androidId;
		    UUID uuid = UUID.nameUUIDFromBytes(UID.getBytes());
		    UID = uuid.toString();
		}
		
		return UID;
	}
	
	
	/**
	 * Creates a cache directory in the external storage.
	 * @return Path of the cache dir
	 */
	public static String createImageCahcheDir(Context context) {
		 String cacheDir = null;
	    	if (Environment.getExternalStorageState().contains(Environment.MEDIA_MOUNTED)) {
	    		
	    		File dir = Environment.getExternalStorageDirectory();
	    			String zoomDealsCache = dir.getAbsolutePath()+ "/.zoomdealsCache";
	    			File zdCacheFile = new File(zoomDealsCache);
	    			
	    			if (!zdCacheFile.exists()){
	    				boolean isCacheDirCreated = zdCacheFile.mkdir();
	    			
	    			if (isCacheDirCreated) 
	    				creteNoMediaFile(zdCacheFile.getAbsolutePath());
	    			
	    			cacheDir = zdCacheFile.getAbsolutePath();
				
	    		}else
	    			cacheDir = zdCacheFile.getAbsolutePath();
	    	}else { //use /data/data/com.mixedmeans.zoomdeals/app_images folder
	    		
	    		File cacheFile = context.getDir("images", Context.MODE_PRIVATE);
	    		cacheDir = cacheFile.getAbsolutePath();
	    	}
	    	
	    return cacheDir;
    }
	
	private static final void creteNoMediaFile(String zdCacheFile) {
		File file = new File(zdCacheFile + "/" + ".nomedia");
		if (!file.exists())
			try {
				file.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
	/**
	 * Saves the bitmap to a file in the given destination	
	 * @param bitmap
	 * @param absoluteDestPath
	 * @return
	 */
	public static boolean saveToFile(Bitmap bitmap, String absoluteDestPath) {
		boolean isFileSaved = false;
		try {
			File newFile = new File(absoluteDestPath);
			FileOutputStream fOut = new FileOutputStream(newFile);
			bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
			fOut.flush();
			fOut.close();
			isFileSaved = true;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO: handle exception
		}

		return isFileSaved;
	}
	
	/*
	 * Draw image in circular shape
	 * Note: change the pixel size if you want image small or
	 * large
	 */
	public static Bitmap getCircleBitmap(Bitmap bitmap, int pixels) {
		Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
				bitmap.getHeight(), Bitmap.Config.ARGB_8888);
		Canvas canvas = new Canvas(output);

		final int color = 0xffff0000;
		final Paint paint = new Paint();
		final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
		final RectF rectF = new RectF(rect);

		paint.setAntiAlias(true);
		paint.setDither(true);
		paint.setFilterBitmap(true);
		canvas.drawARGB(0, 0, 0, 0);
		paint.setColor(color);
		canvas.drawOval(rectF, paint);

		paint.setColor(Color.BLUE);
		paint.setStyle(Paint.Style.STROKE);
		paint.setStrokeWidth((float) 4);
		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		canvas.drawBitmap(bitmap, rect, rect, paint);

		return output;
	}
}
