package au.com.alphsigma.intouch;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Switch;
import android.widget.TextView;
import au.com.alphsigma.intouch.views.Titlebar;
import au.com.alphsigma.intouch.views.Titlebar.TitlebarItemClickListener;

public class FilterActivity extends Activity {
	private Switch sw, sw_conatct, sw_email, sw_icon;
	private ShakeListener mShaker;
	private Button shakeBtn ;
	private TextView back_Tv;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.filter_layout);
		
		back_Tv = (TextView) findViewById(R.id.back_tv);
		shakeBtn = (Button) findViewById(R.id.shake_button);
		sw = (Switch) findViewById(R.id.switch1);
		sw_conatct = (Switch) findViewById(R.id.switch_conatct);
		sw_email = (Switch) findViewById(R.id.switch_email);
		sw_icon = (Switch) findViewById(R.id.switch_icon);

		sw.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView,  boolean ischecked) {
				if (ischecked) {
					// Toast.makeText(getApplicationContext(), "Switch on", Toast.LENGTH_LONG).show();
				} else {
					// Toast.makeText(getApplicationContext(), "Switch off", Toast.LENGTH_LONG).show();
				}
			}
		});

		sw_conatct.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView,  boolean ischecked) {
				if (ischecked) {
					// Toast.makeText(getApplicationContext(), "Switch on", Toast.LENGTH_LONG).show();
				} else {
					// Toast.makeText(getApplicationContext(), "Switch off", Toast.LENGTH_LONG).show();
				}
			}
		});

		sw_email.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView,  boolean ischecked) {
				if (ischecked) {
					// Toast.makeText(getApplicationContext(), "Switch on", Toast.LENGTH_LONG).show();
				} else {
					// Toast.makeText(getApplicationContext(), "Switch off", Toast.LENGTH_LONG).show();
				}
			}
		});

		sw_icon.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView,  boolean ischecked) {
				if (ischecked) {
					// Toast.makeText(getApplicationContext(), "Switch on", Toast.LENGTH_LONG).show();
				} else {
					// Toast.makeText(getApplicationContext(), "Switch off", Toast.LENGTH_LONG).show();
				}
			}
		});
		back_Tv.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				// overridePendingTransition( R.anim.slide_in_right, R.anim.slide_out_right );
				finish();
			}
		});
		shakeBtn.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View arg0) {
				// TODO Auto-generated method stub
				OpenFriendrequest();
				return false;
			}
		});
		mShaker = new ShakeListener(this);
		mShaker.setOnShakeListener(new ShakeListener.OnShakeListener() {
			public void onShake() {
				mShaker.setOnShakeListener(null);
				OpenFriendrequest();
			}
		});
		
		Titlebar titlebar = (Titlebar) findViewById(R.id.title_bar);
		titlebar.showOnlyHomeBackIcon();
		titlebar.setTitle(getString(R.string.tapit));
		
		titlebar.setTitlebarITemClickListener(new TitlebarItemClickListener() {
			@Override
			public void onItemClicked(View view) {
				finish();
			}
		});
	}

	protected void OpenFriendrequest() {
		// TODO Auto-generated method stub
		Intent inte = new Intent( FilterActivity.this, FriendRequestActivity.class );              
		startActivity(inte);
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
		finish();
	}
	
}