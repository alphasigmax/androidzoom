package au.com.alphsigma.intouch;

import android.graphics.Bitmap;

/**
 * interface used to send the response back on image download or upload
 * @author Ravinder
 *
 */
public interface ImageDownloadListener {
	public void onComplete(Bitmap bmp);
	public void onError(String msg);
}
