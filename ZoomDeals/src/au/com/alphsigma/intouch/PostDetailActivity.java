package au.com.alphsigma.intouch;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.TextView;
import au.com.alphsigma.intouch.model.Post;
import au.com.alphsigma.intouch.views.HomePage;
import au.com.alphsigma.intouch.views.Titlebar;
import au.com.alphsigma.intouch.views.Titlebar.TitlebarItemClickListener;

public class PostDetailActivity extends Activity{
	int[][] imgArr = {
			{
				R.drawable.indexasd,
				R.drawable.thumb_one,
				R.drawable.thumb_two,
				R.drawable.thumb_three,
				R.drawable.thumb_four},
				{R.drawable.indexasd,
				}
	};
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.post_detail_activity);
		TextView txtUserName = (TextView) findViewById(R.id.nameUserPostDetail);
		TextView txtPostDate = (TextView) findViewById(R.id.datePostDetail);
		TextView txtUserType = (TextView) findViewById(R.id.typeUserDetail);
		TextView txtPost     = (TextView) findViewById(R.id.postContentPostDetail);
		//Button   btnAddComment  = (Button) findViewById(R.id.btnAddComment);
		Titlebar titlebar 		= (Titlebar) findViewById(R.id.title_bar);
		Gallery imgView		= (Gallery)findViewById(R.id.imgPostDetail);
		//VideoView videoView     = (VideoView) findViewById(R.id.vidPostDetail);
		titlebar.showOnlyHomeIcon();
		titlebar.setTitle("Post Detail");

		titlebar.setTitlebarITemClickListener(new TitlebarItemClickListener() {
			@Override
			public void onItemClicked(View view) {
				finish();
			}
		});
		Post post = HomePage.objHomePage.postList.get(getIntent().getIntExtra("index", 0));
		txtUserName.setText(post.getUserName());
		txtPostDate.setText(post.getPostType());
		txtUserType.setText(post.getUserType());
		txtPost.setText(post.getUserPost());
		// String path = "android.resource://" + getPackageName() + "/" + R.raw.sample;
		//videoView.setVideoURI(Uri.parse(path));
		if(post.getMediaType() == 0){
			imgView.setVisibility(View.VISIBLE);
			imgView.setAdapter(new GalleryImageAdapter(this,imgArr[1]));
		}else if(post.getMediaType() == 1){
			//videoView.setVisibility(View.VISIBLE);
			//videoView.start();
		}else if(post.getMediaType() == 3){
			imgView.setVisibility(View.VISIBLE);
			imgView.setAdapter(new GalleryImageAdapter(this,imgArr[0]));
		}
	}
	public class GalleryImageAdapter extends BaseAdapter 
	{
		private Context mContext;

		private int[] mImageIds;
		private int position;
		public GalleryImageAdapter(Context context, int[] imageArr) {
			mContext 	   = context;
			this.mImageIds = imageArr;
			this.position  = position;
		}

		public int getCount() {
			return mImageIds.length;
		}

		public Object getItem(int position) {
			return position;
		}

		public long getItemId(int position) {
			return position;
		}

		// Override this method according to your need
		public View getView(int index, View view, ViewGroup viewGroup) 
		{
			// TODO Auto-generated method stub
			ImageView i = new ImageView(mContext);

			i.setImageResource(mImageIds[index]);
			// i.setLayoutParams(new Gallery.LayoutParams(420, LayoutParams.WRAP_CONTENT));
			// i.setTag(position);

			return i;
		}
	}

}
