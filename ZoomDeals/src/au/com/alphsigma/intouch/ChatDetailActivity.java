package au.com.alphsigma.intouch;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import au.com.alphsigma.intouch.views.Titlebar;

public class ChatDetailActivity extends Activity{
	String myUserName = "me";
	LinearLayout.LayoutParams paramLeft, paramRight;
	ArrayList<HashMap<String, String>> chatHistoryList = new ArrayList<HashMap<String,String>>();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.chat_detail);
		Titlebar localTitlebar = (Titlebar)findViewById(R.id.title_bar);
		localTitlebar.showChatIcons();
	    //localTitlebar.setTitle("Post Detail");
	    localTitlebar.setTitlebarITemClickListener(new Titlebar.TitlebarItemClickListener(){
		      public void onItemClicked(View paramView){
		    	  ChatDetailActivity.this.finish();
		      }
	    });
	   
	    localTitlebar.setTitleName(getIntent().getStringExtra("name"),getIntent().getStringExtra("status"));
	    localTitlebar.setTitleImage(getIntent().getIntExtra("img", R.drawable.thumb_five));
	    paramLeft = new LinearLayout.LayoutParams(
	    			LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
	    paramLeft.gravity = Gravity.LEFT;
	    paramLeft.setMargins(4, 2, 50, 2);
	    paramRight = new LinearLayout.LayoutParams(
	    		LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
	    paramRight.gravity = Gravity.RIGHT;
		paramRight.setMargins(50,2, 4, 2);
		HashMap<String, String> map;
		for(int i = 0; i < 1 ; i ++){
			map = new HashMap<String, String>();
			map.put("text", "But");
			map.put("time", "17:33");
			map.put("user", "me");
			chatHistoryList.add(map);
			map = new HashMap<String, String>();
			map.put("text", "That would be lot");
			map.put("time", "17:33");
			map.put("user", "notme");
			chatHistoryList.add(map);
			map = new HashMap<String, String>();
			map.put("text", "You gotta help me out with driving the van");
			map.put("time", "17:33");
			map.put("user", "me");
			chatHistoryList.add(map);
			map = new HashMap<String, String>();
			map.put("text", "Where do you want me to drive?");
			map.put("time", "17:34");
			map.put("user", "notme");
			chatHistoryList.add(map);
			map = new HashMap<String, String>();
			map.put("text", "Can you drive to a location and pick up a washing machine in the van and bring it back?");
			map.put("time", "17:34");
			map.put("user", "me");
			chatHistoryList.add(map);
			map = new HashMap<String, String>();
			map.put("text", "Its close by around 20 kms");
			map.put("time", "17:34");
			map.put("user", "me");
			chatHistoryList.add(map);
			map = new HashMap<String, String>();
			map.put("text", "Where abouts");
			map.put("time", "17:35");
			map.put("user", "notme");
			chatHistoryList.add(map);
			map = new HashMap<String, String>();
			map.put("text", "Problem is that i don't have license");
			map.put("time", "17:35");
			map.put("user", "notme");
			chatHistoryList.add(map);
			ListView lvChat 		  = (ListView) findViewById(R.id.lv_chattting);
			ChatUpdateAdapter adapter = new ChatUpdateAdapter(this,chatHistoryList);
			lvChat.setAdapter(adapter);
		}
	}
	  class ChatUpdateAdapter extends BaseAdapter{
	    	private LayoutInflater inflater=null;
	    	ArrayList<HashMap<String, String>> chatList;
	    	public ChatUpdateAdapter(Context ctx, ArrayList<HashMap<String, String>> list) {
	    		// TODO Auto-generated constructor stub
	    		this.chatList = list;
	    		inflater 	  = (LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    	}
	    	public int getCount() {
	    		// TODO Auto-generated method stub
	    		return chatList.size();
	    	}
	    	
	    	public Object getItem(int arg0) {
	    		// TODO Auto-generated method stub
	    		return chatList.get(arg0);
	    	}
	    	 
	    	public long getItemId(int arg0) {
	    		// TODO Auto-generated method stub
	    		return 0;
	    	}
	    	 
	    	public View getView(int arg0, View convertView, ViewGroup arg2) {
	    		// TODO Auto-generated method stub
	    		 final ViewHolder holder;
	    	        if(convertView==null) {
	    	        	convertView 	   = inflater.inflate(R.layout.chatting_row, null);
	    	 	        holder 			   = new ViewHolder();
	    	 	        //holder.friendChat  = (TextView)  convertView.findViewById(R.id.friendChat);
	    	 	        holder.myChat      = (TextView)  convertView.findViewById(R.id.myChat);
	    	        	
	    	        	convertView.setTag(holder);
	                } else {
	                    holder = (ViewHolder) convertView.getTag();
	                }
	    	        HashMap<String, String> map = chatList.get(arg0);
	    	        holder.myChat.setVisibility(View.VISIBLE);
    	        	holder.myChat.setText(map.get("text"));
	    	       if(map.get("user").equalsIgnoreCase(myUserName)){
	    	        	holder.myChat.setBackgroundResource(R.drawable.bubble_green);
	    	        	holder.myChat.setLayoutParams(paramRight);
	    	        }else{
	    	        	
	    	        	holder.myChat.setBackgroundResource(R.drawable.bubble_yellow);
	    	        	holder.myChat.setLayoutParams(paramLeft);
	    	        }
	    	        return convertView;
	    	}
	    	protected class ViewHolder {
	            //TextView friendChat;
	            TextView myChat;
	        }
	    }
}
