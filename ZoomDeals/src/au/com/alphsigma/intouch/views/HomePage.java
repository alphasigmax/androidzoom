package au.com.alphsigma.intouch.views;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import au.com.alphsigma.intouch.MyProfileActivity;
import au.com.alphsigma.intouch.PostDetailActivity;
import au.com.alphsigma.intouch.R;
import au.com.alphsigma.intouch.model.Comments;
import au.com.alphsigma.intouch.model.Post;



public class HomePage extends Page implements OnItemClickListener, OnClickListener{
	//private AnimatedListView animatedListView;
	public static HomePage objHomePage;
	public List<Post> postList;
	Context context;
	//private LinearLayout homePageLayout;
	public HomePage(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		objHomePage  = this;
		postList = new ArrayList<Post>();
		Post post ;
		String lorem = "Check out the new pictures released by NASA for our stuff. When we are born, the deoxyribonucleic acid/DNA in our bodies contains the blueprints for who we are and instructions for who we will become. For example, it can tell our eyes to eventually turn from blue at birth to hazel later on, our length to grow from 20 inches to 70 and direct a multitude of other changes over the course of our lives. ";
		for(int i = 0 ; i <8 ; i++){
			post = new Post();
			post.setUserName("Felix");
			post.setUserType(" ");
			post.setUserImg("path/to/image");
			post.setUserPost(lorem);
			if(i%3 == 1){
				post.setMediaType(0);
			}else if(i%2==0){
				post.setMediaType(1);
			}else if(i%4==0){
				post.setMediaType(3);
			}
			else{
				post.setMediaType(2);
			}
			post.setPostImg("path/to/post/image");
			post.setPostVid("path/to/post/video");
			post.setPostPlus("+1");
			post.setPostType("Friends -" + (1 + i) * (1+((int)(Math.random()*  10))) +" hours ago");
			post.setPostCommentCount( "" +( 5+ i )* ((int)(Math.random()* 10)));
			Comments comments = new Comments();
			
			comments.set_comment("This is cool! I wish NASA got more funding ");
			comments.set_img("comment/user/img");
			comments.setName("John");
			List<Comments> commentList = new ArrayList<Comments>();
			commentList.add(comments);
			post.setComments(commentList);
			postList.add(post);
		//============= 2
			
			lorem="\"I have not failed. I've just found 10,000 ways that won't work.\" - Thomas Edison. Thanks to Mr. Edison's perseverance and creativity, there are about 12 billion light bulbs worldwide! ";
				
			post = new Post();
			post.setUserName("Sarah");
			post.setUserType(" ");
			post.setUserImg("path/to/image");
			post.setUserPost(lorem);
			if(i%3 == 1){
				post.setMediaType(0);
			}else if(i%2==0){
				post.setMediaType(1);
			}else if(i%4==0){
				post.setMediaType(3);
			}
			else{
				post.setMediaType(2);
			}
			post.setPostImg("path/to/post/image");
			post.setPostVid("path/to/post/video");
			post.setPostPlus("+1");
			post.setPostType("Family -" + (1 + i) * (1+((int)(Math.random()*  10))) +" hours ago");
			post.setPostCommentCount( "" +( 5+ i )* ((int)(Math.random()* 10)));
			 comments = new Comments();
			
			comments.set_comment("Did he invent HMV?");
			comments.set_img("comment/user/img");
			comments.setName("John");
			commentList = new ArrayList<Comments>();
			commentList.add(comments);
			post.setComments(commentList);
			postList.add(post);
			
			
			//================== 3
			
			
				
			post = new Post();
			post.setUserName("Felix");
			post.setUserType(" ");
			post.setUserImg("path/to/image");
			post.setUserPost(lorem);
			if(i%3 == 1){
				post.setMediaType(0);
			}else if(i%2==0){
				post.setMediaType(1);
			}else if(i%4==0){
				post.setMediaType(3);
			}
			else{
				post.setMediaType(2);
			}
			post.setPostImg("path/to/post/image");
			post.setPostVid("path/to/post/video");
			post.setPostPlus("+1");
			post.setPostType("Friends -" + (1 + i) * (1+((int)(Math.random()*  10))) +" hours ago");
			post.setPostCommentCount( "" +( 5+ i )* ((int)(Math.random()* 10)));
			 comments = new Comments();
			
			comments.set_comment(" ");
			comments.set_img("comment/user/img");
			comments.setName("John");
			 commentList = new ArrayList<Comments>();
			commentList.add(comments);
			post.setComments(commentList);
			postList.add(post);
			
		}
	}

	@Override
	public void setContent() {
		super.setContent();
		RelativeLayout pageLayout =  (RelativeLayout) inflate(getContext(), R.layout.home_page, null);
		pageLayout.removeAllViews();
		

		ListView listView 			= (ListView)inflate(context, R.layout.posts_listview, null) ;
		listView.setOnItemClickListener(this);
		PostListAdapter adapter     = new PostListAdapter(postList, getContext());
		listView.setAdapter(adapter);
		pageLayout.addView(listView);
		addView(pageLayout);
		
	}
	
		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub
			Toast.makeText(getContext(), "Click", 4000).show();
			if(postList.get(arg2).getMediaType() == 1){
				getContext().startActivity(new Intent(Intent.ACTION_VIEW,Uri.parse("https://www.youtube.com/watch?v=Uj56IPJOqWE")));
			}else{
				getContext().startActivity(new Intent(getContext(), PostDetailActivity.class).putExtra("index", arg2));
			}
			
		}
		
		
		class PostListAdapter extends BaseAdapter{
			List<Post> postsList = new ArrayList<Post>();
			private LayoutInflater mInflater=null;
			private int lastPosition = -1;
			private int[][] imgArr = {
										{
							    		R.drawable.indexasd,
							            R.drawable.thumb_one,
							            R.drawable.thumb_two,
							            R.drawable.thumb_three,
							            R.drawable.thumb_four},
							            {R.drawable.indexasd,
							            }
							         };
			public PostListAdapter(List<Post> list, Context ctx) {
				// TODO Auto-generated constructor stub
				this.postsList = list;
				mInflater = (LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			}
			@Override
			public int getCount() {
				// TODO Auto-generated method stub
				return this.postsList.size();
			}

			@Override
			public Object getItem(int arg0) {
				// TODO Auto-generated method stub
				return this.postsList.get(arg0);
			}

			@Override
			public long getItemId(int arg0) {
				// TODO Auto-generated method stub
				return 0;
			}

			@Override
			public View getView(int position, View view, ViewGroup arg2) {
				PostsViewHolder viewHolder;
				
			    if(view == null){
			        view 				   		= mInflater.inflate(R.layout.post_list_item,arg2,false);
			 
			        viewHolder 			   		= new PostsViewHolder();
			        viewHolder.txtUserName 		= (TextView)view.findViewById(R.id.nameUserPost);
			        viewHolder.txtPostDate 		= (TextView)view.findViewById(R.id.datePost);
			        viewHolder.txtUserType 		= (TextView)view.findViewById(R.id.typeUser);
			        viewHolder.txtPost 	   		= (TextView)view.findViewById(R.id.postContent);
			        viewHolder.txtTotalComment 	= (TextView)view.findViewById(R.id.totalCommentPost);
			        viewHolder.txtPlusCout 	   	= (TextView)view.findViewById(R.id.plusPost);
			        viewHolder.txtComment 	   	= (TextView)view.findViewById(R.id.commentByUserOnPost);
			        viewHolder.imgPost			= (Gallery) view.findViewById(R.id.postImage);
			        viewHolder.imgUser			= (ImageView) view.findViewById(R.id.imgUserPost);
			        viewHolder.vidLayout    	= (RelativeLayout) view.findViewById(R.id.postVidLayout);
			        viewHolder.plusPostLayout   = (LinearLayout) view.findViewById(R.id.plusPostLayout);
			        viewHolder.plusPostLayout.setTag(position);
			        viewHolder.txtPlusCout.setTag(position);
			        viewHolder.imgUser.setTag(position);
			        viewHolder.txtPost.setTag(position+100);
			        viewHolder.txtComment.setTag(position+1000);
			        view.setTag(viewHolder);
			    }else{
			        viewHolder = (PostsViewHolder)view.getTag();
			    }
			 
			    Post post = (Post) postsList.get(position);
			    viewHolder.txtUserName.setText(post.getUserName());
			    viewHolder.txtPostDate.setText(post.getPostType());
			    viewHolder.txtUserType.setText(post.getUserType());
			    viewHolder.txtPost.setText(post.getUserPost());
			    viewHolder.txtTotalComment.setText(post.getPostCommentCount());
			    viewHolder.txtPlusCout.setText(post.getPostPlus());
			    
			    
				StyleSpan boldSpan 		  = new StyleSpan(Typeface.BOLD);
				SpannableString spannable = new SpannableString(post.getComments().get(0).getName()+" "+post.getComments().get(0).get_comment());	
				spannable.setSpan(boldSpan,0, post.getComments().get(0).getName().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
				
			    viewHolder.txtComment.setText(spannable);
		        if(view != null) {
		        	 Animation animation = new TranslateAnimation(0, 0, (position > lastPosition) ? 180 : -180, 0);
		        	 animation.setDuration(500);
		             view.startAnimation(animation);
		        }
		        if(post.getMediaType() == 0){
		        	viewHolder.imgPost.setVisibility(View.VISIBLE);
		        	if(position%4 == 0){
		        		post.setMediaType(3);
		        		viewHolder.imgPost.setAdapter(new GalleryImageAdapter(getContext(),imgArr[0], position));
		        	}else{
		        		viewHolder.imgPost.setAdapter(new GalleryImageAdapter(getContext(),imgArr[1], position));
		        	}
		        	
		        	viewHolder.vidLayout.setVisibility(View.GONE);
		        }else if(post.getMediaType() == 1 ){
		        	viewHolder.vidLayout.setVisibility(View.VISIBLE);
		        	viewHolder.imgPost.setVisibility(View.GONE);
		        }else{
		        	viewHolder.vidLayout.setVisibility(View.GONE);
		        	viewHolder.imgPost.setVisibility(View.GONE);
		        }
		        viewHolder.imgUser.setOnClickListener(HomePage.this);
		        lastPosition = position;
		        viewHolder.plusPostLayout.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						LinearLayout layout = (LinearLayout) v;
						TextView tv = (TextView) layout.getChildAt(0);
						if(tv.getText().toString().equalsIgnoreCase("+1")){
							layout.setBackgroundDrawable(getResources().getDrawable(R.drawable.rounded_red));
							tv.setText("+2");
							tv.setTextColor(Color.WHITE);
						}else{
							layout.setBackgroundDrawable(getResources().getDrawable(R.drawable.rounded_white));
							tv.setText("+1");
							tv.setTextColor(Color.GRAY);
						}
					}
				});
		        viewHolder.imgPost.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {
						// TODO Auto-generated method stub
						getContext().startActivity(new Intent(getContext(), PostDetailActivity.class).putExtra("index", (Integer)arg1.getTag()));
					}
				});
		       
			    return view;
			}
			class PostsViewHolder{
			        TextView       txtUserName;
			        TextView       txtPostDate;
			        TextView       txtUserType;
			        TextView       txtPost;
			        TextView       txtTotalComment;
			        TextView       txtPlusCout;
			        TextView       txtComment;
			        Gallery        imgPost;
			        ImageView	   imgUser;
			        RelativeLayout vidLayout;
			        LinearLayout   plusPostLayout;
			}
		}


		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			int position = (Integer)v.getTag();
			getContext().startActivity(new Intent(getContext() , MyProfileActivity.class).putExtra("index", position));
			
		}
		public class GalleryImageAdapter extends BaseAdapter 
		{
		    private Context mContext;

		    private int[] mImageIds;
		    private int position;
		    public GalleryImageAdapter(Context context, int[] imageArr,int position) 
		    {
		        mContext 	   = context;
		        this.mImageIds = imageArr;
		        this.position  = position;
		    }

		    public int getCount() {
		        return mImageIds.length;
		    }

		    public Object getItem(int position) {
		        return position;
		    }

		    public long getItemId(int position) {
		        return position;
		    }


		    // Override this method according to your need
		    public View getView(int index, View view, ViewGroup viewGroup) 
		    {
		        // TODO Auto-generated method stub
		        ImageView i = new ImageView(mContext);
		       
		        i.setImageResource(mImageIds[index]);
		       // i.setLayoutParams(new Gallery.LayoutParams(420, LayoutParams.WRAP_CONTENT));
		        i.setTag(position);
		        //i.setScaleType(ImageView.ScaleType.FIT_XY);
//		        i.setOnClickListener(new OnClickListener() {
//					
//					@Override
//					public void onClick(View v) {
//						// TODO Auto-generated method stub
//						getContext().startActivity(new Intent(getContext(), PostDetailActivity.class).putExtra("index", (Integer)v.getTag()));
//					}
//				});
		        return i;
		    }
		}
}
