package au.com.alphsigma.intouch.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import au.com.alphsigma.intouch.R;



/**
 * Title bar of the application. 
 * Has 4 buttons and the interface for the click listeners of these button
 * @author Ravinder
 *
 */
public class Titlebar extends RelativeLayout implements OnClickListener{

	/**
	 * Implement this interface to receive the 
	 * click events of the buttons on the titlebar
	 * @author Ravinder
	 *
	 */
	public interface TitlebarItemClickListener{
		public void onItemClicked(View view);
	}
	
	private TitlebarItemClickListener mClickListener;
	
	public void setTitlebarITemClickListener(TitlebarItemClickListener l) {
		mClickListener = l;
	}
	
	
	
	public Titlebar(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();
		initViews();
	}
	
	private void initViews() {
		//findViewById(R.id.title_more_btn).setOnClickListener(this);
		findViewById(R.id.title_home_btn).setOnClickListener(this);
		findViewById(R.id.title_menu_btn).setOnClickListener(this);
		findViewById(R.id.title_refresh_btn).setOnClickListener(this);
	}
	
	@Override
	public void onClick(View v) {
		if (mClickListener != null) 
			mClickListener.onItemClicked(v);
		
	}
	
	public void showOnlyHomeIcon() {

		//findViewById(R.id.gps_btn).setVisibility(View.GONE);
		findViewById(R.id.audio_btn).setVisibility(View.GONE);
		findViewById(R.id.video_btn).setVisibility(View.GONE);
		
	
	}
	
	public void showOnlyHomeBackIcon() {
		findViewById(R.id.title_menu_btn).setVisibility(View.GONE);
		findViewById(R.id.title_refresh_btn).setVisibility(View.GONE);
	//	findViewById(R.id.gps_btn).setVisibility(View.GONE);
		findViewById(R.id.audio_btn).setVisibility(View.GONE);
		findViewById(R.id.video_btn).setVisibility(View.GONE);
		
	
	}
	
	public void showChatIcons() {
//			findViewById(R.id.title_whats_new_btn).setVisibility(View.VISIBLE);
//		findViewById(R.id.title_whats_hot_btn).setVisibility(View.VISIBLE);
//			findViewById(R.id.title_deals_for_me).setVisibility(View.VISIBLE);
		findViewById(R.id.title_refresh_btn).setVisibility(View.GONE);	
		}
	
	
	public void setTitle(String title) {
		TextView view = (TextView) findViewById(R.id.titlebar_title);
		view.setText(title);
		view.setVisibility(View.VISIBLE);
		
	}
	public void setTitleName(String title, String userStatus) {
		TextView view   = (TextView) findViewById(R.id.title_bar_user_name);
		TextView status = (TextView) findViewById(R.id.title_bar_user_status);
		view.setText(title);
		status.setText(userStatus);
		view.setVisibility(View.VISIBLE);
		status.setVisibility(View.VISIBLE);
		
	}
	
	public void setTitleImage(int img) {
		ImageView view = (ImageView) findViewById(R.id.title_bar_home_icon);
		//view.setBackgroundResource(img);
		view.setImageResource(img);
		view.setVisibility(View.VISIBLE);
		
	}
}
