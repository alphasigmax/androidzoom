package au.com.alphsigma.intouch.views;

import java.util.ArrayList;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.text.Html;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import au.com.alphsigma.intouch.ChatDetailActivity;
import au.com.alphsigma.intouch.R;
import au.com.alphsigma.intouch.Utils;
import au.com.alphsigma.intouch.model.Chat;



public class ChatsPage extends Page implements OnScrollListener {

	/**
	 * Interface to listen to the data request from the view.
	 * @author Ravinder
	 *
	 */
		public static ChatsPage objChatPAge;
	public interface ListPageDataRequestListener{
		public void onListDataRequest(ChatsPage page);
		public void onRequestMoreData();
	}
	ListPageDataRequestListener mDataListener;
	public void setListDataListener(ListPageDataRequestListener l) {
		mDataListener = l;
		
	}
	
	private ListView mChatsListView;
	private ChatsListAdapter mChatsAdapter;
	private ArrayList<Chat> mChats = new ArrayList<Chat>();
	//private LinearLayout mProgress;
	
	public ChatsPage(Context context, AttributeSet attrs) {
		super(context, attrs);
		objChatPAge = this;
		mChats.clear();
		mChats.addAll(parse());
		
	}
	
	@Override
	public void setContent() {
		super.setContent();
		RelativeLayout pageLayout =  (RelativeLayout) inflate(getContext(), R.layout.page, null);
		pageLayout.removeAllViews();
		mChatsListView = (ListView) inflate(getContext(), R.layout.chats_listview, null);
		mChatsListView.setScrollingCacheEnabled(false);
		mChatsListView.setOnScrollListener(this);
		mChatsListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				mChats.get(arg2).mIsRead = true;
				String isOnline = mChats.get(arg2).mIsOnline?"online":"offline";
				getContext().startActivity(new Intent(getContext() , ChatDetailActivity.class).putExtra("name", mChats.get(arg2).mFrom).putExtra("img", Utils.thumbs[arg2]).putExtra("status", isOnline));
				
			}
		});
		mChatsAdapter = new ChatsListAdapter();
		mChatsListView.setAdapter(mChatsAdapter);
		pageLayout.addView(mChatsListView);
		addView(pageLayout);
		requestData();
		
	}
	
	private void requestData() {
		if (mDataListener != null)
			mDataListener.onListDataRequest(this);
		
	}
	
	public void setChats(ArrayList<Chat> chats){
		
		//mRefreshListener = refreshListener;
		mChats.addAll(chats);
		mChatsAdapter.notifyDataSetChanged();
		
		Log.d("ChatsPage", "=====setChats====");
	}
	
	public void refreshList(Chat chat) {
		if (mChatsAdapter != null && mChats.contains(chat)) {
			int position = mChats.indexOf(chat);
			int firstVisibleItem = mChatsListView.getFirstVisiblePosition();
			int lastVisibleItem = mChatsListView.getLastVisiblePosition();
			if (position >= firstVisibleItem && position <= lastVisibleItem)
				mChatsAdapter.notifyDataSetChanged();
		}
	}
	
	public void refreshList() {
		mStopRefreshing = false;
		if (mChatsAdapter != null)
			mChatsAdapter.notifyDataSetChanged();
	}
	
	
	public static boolean mStopRefreshing = false;
	//When sidebar is animating then stop refreshing the listview by the system
	private boolean isStopRefreshing() {
		return mStopRefreshing;
	}
	
	@Override
	public void onScroll(AbsListView listView, int firstVisibleItem, 
						int visibleItemCount, int totalItemCount) {
					// TODO Auto-generated method stub
		
	}

	public int mScrollState = AbsListView.OnScrollListener.SCROLL_STATE_IDLE;
	@Override
	public void onScrollStateChanged(AbsListView listView, int scrollState) {
		mStopRefreshing = false;
	}
	
	/**
	 * View cache mechanism for the smooth scrolling of the listview
	 * @author Ravinder
	 *
	 */
   private static class ViewHolder{
        public TextView mTitleView;
        public TextView mDateTime;
        public TextView mChatTxt;
        public ImageView mUserImg;
        public ImageView mStar;
        //public RelativeLayout relative;
        
    }
	
   /**
    * List adapter for the Chats list
    * @author Ravinder
    *
    */
	private class ChatsListAdapter extends BaseAdapter {
		int lastPosition = -1;
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return mChats.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return mChats.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
					
			ViewHolder holder;
					
			if (convertView == null) {
				 convertView = inflate(getContext(), R.layout.chats_list_item, null);
				 holder = new ViewHolder();
				 setViewsToHolder(convertView, holder);
				 convertView.setTag(holder);
			}
			else {
				holder = (ViewHolder) convertView.getTag();
			}
			if(convertView != null) {
	            Animation animation = new TranslateAnimation(0, 0, (position > lastPosition) ? 80 : -80, 0);
	            animation.setDuration(500);
	            convertView.startAnimation(animation);
	        }
	        lastPosition = position;
			if (!isStopRefreshing())
				updateChatsItem(holder, position);
			return convertView;
		}
		
		
		private void setViewsToHolder(View dealsItemView, ViewHolder holder) {
			//holder.relative			= (RelativeLayout) dealsItemView.findViewById(R.id.layout_chat_list);
			holder.mTitleView 		= (TextView)  dealsItemView.findViewById(R.id.chat_list_user_info);
		 	holder.mDateTime 		= (TextView)  dealsItemView.findViewById(R.id.chat_list_date_time);
		 	holder.mChatTxt 		= (TextView)  dealsItemView.findViewById(R.id.chat_list_text);
		 	holder.mUserImg 		= (ImageView) dealsItemView.findViewById(R.id.chat_list_user_img);
		 	holder.mStar 			= (ImageView) dealsItemView.findViewById(R.id.chat_list_star_img);
		 	
		}
		
		@SuppressLint("NewApi")
		private void updateChatsItem(ViewHolder holder, int position) {
			Chat chat = mChats.get(position);
			
			holder.mTitleView.setText(Html.fromHtml(chat.mFrom + " " + chat.mTo));
			holder.mDateTime.setText(chat.mDateCreated);
			holder.mChatTxt.setText(chat.mChatText);
		 	//setLogoImage(holder, chat);
			
			holder.mUserImg.setBackgroundResource(Utils.thumbs[position]);
			holder.mTitleView.setTypeface(holder.mTitleView.getTypeface(), Typeface.BOLD);
//			if(chat.mIsRead){
//				holder.relative.setBackgroundColor(getResources().getColor(R.color.chat_list_read_bg));
//				
//			}else{
//				
//				holder.relative.setBackgroundColor(getResources().getColor(R.color.posts_comment_bg));
//			}
			
			if (position % 4 == 0) {
				holder.mStar.setImageResource(R.drawable.greencheck);
			} else
			if (position % 3 == 0) {
				holder.mStar.setImageResource(R.drawable.greencheck);
			} else
			if (position % 2 == 0) {
				holder.mStar.setImageResource(R.drawable.greencheck); 
			} else
			if (position % 5 == 0) {
				holder.mStar.setImageResource(R.drawable.hourglass);
			}
			else
			{
			holder.mStar.setImageResource(R.drawable.check);
			}
		}
		
		
//		private void setLogoImage(ViewHolder holder, Chat chat) {
//		
//			if (deal.getLogoImageUri() == null)
//				deal.fetchLogo();
//			else {
//				if (deal.getLogoImageUri() != null) {
//					deal.mIsLogoSet = true;
//					holder.mStar.setImageURI(deal.getLogoImageUri());
//					holder.mProgressBar.setVisibility(GONE);
//					//holder.mStar.setVisibility(VISIBLE);
//				}else 
//					deal.fetchLogo();
//			}
//		} 	
		
	}
	private ArrayList<Chat> parse() {
		String[] chats = new String[]{
				" You reckon?",
				"Word bro, we got to setup up the whole stuff by tonight ...",
				"Funky?? Who uses that word anymore?",
				"I'll think about decorations for the party . Does anybody have more...",
				"I have a date with destiny at the top floor man",
				"Brooklyn was awesome. My neighbours too",
				"A'ight man!",
				"Here are the details, please book  your ticket before 10th of next month",
				"Party time! I mean tupperware party.",
				"Definitely we should work on the spacing across all elements, not just..",
				"Man, no one invited me to valentine's day. What should I do?",
				"We at the surf festival on bells beach. Hang with us?",
				"Oh wow! Good for him",
				"We are at the entrance close to visitor information booth",
				"I tell you it's not a conspiracy theory. It's true!",
				
		}; 
		String[] from = new String[]{
				"Bobby ",
				"Dave",
				"Hans",
				"Oliver",
				"April",
				"Kalyan",
				"Adam",
				"Michael",
				"Joe ",
				"Dave",
				"Ganesh",
				"Mimi",
				"Sarah",
				"Felix",
				"Dennis",
				
		};
		String[] to = new String[]{
				" Madelung",
				" Hartmann",
				"Joseph",
				"Jackson ",
				"Nicole",
				"Parvati",
				" Khalanski",
				" Krishnan",
				" Patel",
				"Smith",
				"Jones",
				"Thomson",
				"Martin",
				"Harris",
				"Vallury",
				
				
		};
		
		ArrayList<Chat> chatList = new ArrayList<Chat>();
				for(int i = 0; i < Utils.thumbs.length; i++) {
					Chat chat 			= new Chat();
					
					chat.mChatID 		= 1;
					//dealPost.mBackgroundURL = adpostJson.getString("background");
					chat.mDateCreated 	=  i +1 + ":" +  ((int) (Math.random()* 59)) +" AM";
					chat.mChatText 		= chats[i];
					chat.mFrom 			= from[i];
					chat.mTo   			= to[i];
					chat.mCount			= 7;
					chat.mIsFavorite 	= true;
					chat.mImageUri   	= Uri.parse("");
					chat.mIsRead 		= false;
					if(i%2 == 0){
						chat.mIsOnline 		= false;
					}else{
						chat.mIsOnline 		= true;
					}
					//dealPost.setRefreshListener(mListener);
					chatList.add(chat);
				}
		
		return chatList;

	}

}
