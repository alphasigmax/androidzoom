package au.com.alphsigma.intouch.views;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import au.com.alphsigma.intouch.R;


public class SidebarItem extends RelativeLayout{

	public SidebarItem(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}
	
	public void setIcon(int imageRes) {
		ImageView iconView = (ImageView) findViewById(R.id.item_icon);
		iconView.setImageResource(imageRes);
	}
	
	public void setTitle(String title) {
		TextView titleView = (TextView) findViewById(R.id.item_title);
		titleView.setText(title);
		
	}

	public void enableToggleBrn() {
		findViewById(R.id.sidebar_item_toggle_btn).setVisibility(VISIBLE);
	}
	
	@Override
	public void setEnabled(boolean enabled) {
		if (enabled == false) {
			TextView titleView = (TextView) findViewById(R.id.item_title);
			titleView.setTextColor(Color.GRAY);
		}
		super.setEnabled(enabled);
	}
	
	public void toggleState() {
		CheckBox checkBox = (CheckBox) findViewById(R.id.sidebar_item_toggle_btn);
		checkBox.toggle();
	}
}
