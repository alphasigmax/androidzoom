package au.com.alphsigma.intouch.views;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import au.com.alphsigma.intouch.R;
import au.com.alphsigma.intouch.Utils;
import au.com.alphsigma.intouch.model.Contact;


public class ContactPage extends Page{	
	RelativeLayout.LayoutParams params;
	Activity activity;
	private ContactsListAdapter mContactsAdapter;
	private ArrayList<Contact> mContacts = new ArrayList<Contact>();

	public static int DEVICE_SCREEN_WIDTH = 320; //default width
	public ContactPage(Activity context, AttributeSet attrs) {
		super(context, attrs);
		mContacts.clear();
		mContacts.addAll(parse());
		activity = context;
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();
	}

	@Override
	public void setContent() {
		if (getChildCount() == 0) {
		}
		DisplayMetrics metrics  = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
		LinearLayout pageLayout =  (LinearLayout) inflate(getContext(), R.layout.contact_page, null);
		GridView gvContacts   	=  (GridView) (pageLayout).findViewById(R.id.contacts_gridview);
		ListView lvContacts1   	=  (ListView) (pageLayout).findViewById(R.id.contacts_listview1);
		ListView lvContacts2   	=  (ListView) (pageLayout).findViewById(R.id.contacts_listview2);
		int totalHeight2 = 0,totalHeight3 = 0;
		ViewGroup.LayoutParams param1, param2 ,param3;

		DEVICE_SCREEN_WIDTH = metrics.widthPixels;
		params = new RelativeLayout.LayoutParams(DEVICE_SCREEN_WIDTH/3, DEVICE_SCREEN_WIDTH/3);

		if(mContacts.size() > 0){
			totalHeight2 = (int) (mContacts.size()*55*this.getResources().getDisplayMetrics().density);
		}
		if(mContacts.size() > 0){
			totalHeight3 = (int) (mContacts.size()*55*this.getResources().getDisplayMetrics().density);
		}

		param1 	      = gvContacts.getLayoutParams();
		param2 	      = lvContacts1.getLayoutParams();
		param3 	      = lvContacts2.getLayoutParams();
		param1.height = (DEVICE_SCREEN_WIDTH/3)*2+10;
		param2.height = totalHeight2 + (lvContacts1.getDividerHeight() * (lvContacts1.getCount() - 1));
		param3.height = totalHeight3 + (lvContacts2.getDividerHeight() * (lvContacts2.getCount() - 1));

		mContactsAdapter = new ContactsListAdapter();
		lvContacts1.setAdapter(mContactsAdapter);
		lvContacts2.setAdapter(mContactsAdapter);
		ContactsGridAdapter mGridAdapter = new ContactsGridAdapter();
		gvContacts.setAdapter(mGridAdapter);
		//pageLayout.addView(mChatsListView);
		gvContacts.setLayoutParams(param1);
		lvContacts1.setLayoutParams(param2);
		lvContacts2.setLayoutParams(param3);

		addView(pageLayout);
		super.setContent();
	}

	/**
	 * List adapter for the Contacts list
	 * @author Ravinder
	 *
	 */
	private class ContactsListAdapter extends BaseAdapter {
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return mContacts.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return mContacts.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			ViewHolder holder;

			if (convertView == null) {
				convertView = inflate(getContext(), R.layout.contacts_list_item, null);
				holder 	 = new ViewHolder();
				setViewsToHolder(convertView, holder);
				convertView.setTag(holder);
			} else {
				holder 		 = (ViewHolder) convertView.getTag();
			}
			updateContactsItem(holder, position);
			return convertView;
		}

		private void setViewsToHolder(View dealsItemView, ViewHolder holder) {
			//holder.relative			= (RelativeLayout) dealsItemView.findViewById(R.id.layout_chat_list);
			holder.mUserName 		= (TextView) dealsItemView.findViewById(R.id.contact_list_user_name);
			holder.mUserEmail 		= (TextView) dealsItemView.findViewById(R.id.contact_list_user_email);
			holder.mUserImg 		= (ImageView) dealsItemView.findViewById(R.id.contacts_list_user_img);
		//	holder.mStatusImg 		= (ImageView) dealsItemView.findViewById(R.id.contact_list_status_img);

		}

		@SuppressLint("NewApi")
		private void updateContactsItem(ViewHolder holder, int position) {
			Contact contact = mContacts.get(position);
			holder.mUserImg.setBackgroundResource(Utils.thumbs[position]);
			holder.mUserName.setText(contact.getUserName());
			holder.mUserName.setTypeface(holder.mUserName.getTypeface(), Typeface.BOLD);
			holder.mUserEmail.setText(contact.getUserEmail());
			if(position%2 == 0){
				//holder.mStatusImg.setBackgroundResource(R.drawable.online);
			}else{
			//	holder.mStatusImg.setBackgroundResource(R.drawable.offline);
			}
		}


	}
	private ArrayList<Contact> parse() {

		String[] user = new String[]{
				"Bobby ",
				"Dave",
				"Oliver",
				"April",
				"Kalyan",
				"Adam",
				"YCricket",
				"Joe ",
				"Dave",

		};
		String[] emails = new String[]{
				"Online via InTouch!",
				"Enjoying life backpacking India",
				"Online via InTouch!",
				"Life is like a box of chocolates...",
				"In a meeting. InTouch only",
				"Busy",
				"Online via InTouch!",
				"Online via InTouch!",
				"Online via Telegram",
		};

		ArrayList<Contact> contactList = new ArrayList<Contact>();
		for(int i = 0; i < 6; i++) {
			Contact contact 			= new Contact();
			contact.setContactId("1");
			contact.setUserName(user[i]);
			contact.setUserEmail(emails[i]);
			contact.setUserImgUrl("url/to/image");

			if(i%2 == 0){
				contact.setAvailable(true);
			} else {
				contact.setAvailable(false);
			}
			contactList.add(contact);
		}
		return contactList;

	}
	private static class ViewHolder{
		public TextView mUserName;
		public TextView mUserEmail;
		public ImageView mUserImg; 
		public ImageView mStatusImg;
		public RelativeLayout relative;

	}
	class ContactsGridAdapter extends BaseAdapter{

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return 6;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return mContacts.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			ViewHolder holder;
			if (convertView == null) {
				convertView = inflate(getContext(), R.layout.contacts_grid_item, null);
				holder 	 = new ViewHolder();
				setViewsToHolder(convertView, holder);
				convertView.setTag(holder);
			} else {
				holder 		 = (ViewHolder) convertView.getTag();
			}

			updateContactsItem(holder, position);
			return convertView;
		}
		private void setViewsToHolder(View dealsItemView, ViewHolder holder) {
			holder.relative			= (RelativeLayout) dealsItemView.findViewById(R.id.relative_gv_item);
			holder.mUserName 		= (TextView) dealsItemView.findViewById(R.id.gv_user_name);
			holder.mUserImg 		= (ImageView) dealsItemView.findViewById(R.id.gv_img_user);
		//	holder.mStatusImg 		= (ImageView) dealsItemView.findViewById(R.id.gv_img_user_status);
		}

		@SuppressLint("NewApi")
		private void updateContactsItem(ViewHolder holder, int position) {
			holder.mUserImg	.setLayoutParams(params);
			Contact contact = mContacts.get(position);
			holder.mUserName.setText(contact.getUserName());
			holder.mUserImg.setBackgroundResource(Utils.thumbs[position]);
			if(position%2 == 0){
				//holder.mStatusImg.setBackgroundResource(R.drawable.online);
			}else{
				//holder.mStatusImg.setBackgroundResource(R.drawable.online);
			}
		}
	}
}
