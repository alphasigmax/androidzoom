package com.mixedmeans.zoomdeals.views;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.TextUtils.TruncateAt;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;

import com.mixedmeans.zoomdeals.R;

public class CustomSpinner extends Button implements View.OnClickListener {

	Object mDropDownItems[]; 
	String mEmptyText, mHeaderText;
	Dialog mSelectionDialog;
	
	OnItemSelectedListener mOnItemSelectedListener;
	private int mSelectedItemPosition;
	
	public CustomSpinner(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	@Override
	protected void onFinishInflate() {
		// A hack to get the same look as native Spinner
		Spinner s = new Spinner(getContext());
		setBackgroundDrawable(s.getBackground());
		setOnClickListener(this);
		mEmptyText = getText().toString();
		mHeaderText = getContext().getString(R.string.select_the_gender);
		setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
		setTextSize(13);
		setMaxLines(1);
		setEllipsize(TruncateAt.END);
		
		Typeface helvetica = Typeface.createFromAsset(getContext().getAssets(), "HelveticaNeue.ttf");
		setTypeface(helvetica);
		
		super.onFinishInflate();
	}
	
	public void setCustomBackground(Drawable drawable) {
		setBackgroundDrawable(drawable);
	}
	
	public void setDropDownItems(Object displayItems[]) {
		final Context context = getContext();
		mDropDownItems = displayItems;
		setText(mEmptyText);
		if (displayItems != null) {
			mSelectedItemPosition = -1;
			final String items[] = new String[displayItems.length];

			for (int i = 0; i < displayItems.length; i++) {
				if (displayItems[i] != null)
					items[i] = displayItems[i].toString();
			}
			
			AlertDialog.Builder builder = new AlertDialog.Builder(context);
			builder.setTitle(mHeaderText);
			builder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int item) {
					mSelectedItemPosition = item;
					setText(items[item]);
//					Toast.makeText(context, items[item], Toast.LENGTH_SHORT).show();
					if (mOnItemSelectedListener != null) {
						mOnItemSelectedListener.OnItemSelected(CustomSpinner.this,
								item, mDropDownItems[item].toString());
						
					}
					if (mItemChooseListener != null)
						mItemChooseListener.OnItemSelected(CustomSpinner.this,
											mDropDownItems[item]);
					
					mSelectionDialog.dismiss();
				}
			});
			mSelectionDialog = builder.create();
		}
	}
	
	public void setOnItemSelectedListener(OnItemSelectedListener itemSelectedListener) {
		mOnItemSelectedListener = itemSelectedListener;
	}

	@Override
	public void onClick(View v) {
		if (mDropDownItems == null || mDropDownItems.length == 0) {
			if (mOnItemSelectedListener != null) {
				mOnItemSelectedListener.OnEmptyStateSelected(CustomSpinner.this);
			}
		} else {
			mSelectionDialog.show();
		}
	}
	
	public int getSelectedItemPosition() {
		return mSelectedItemPosition;
	}
	
	public void setPrompt(String message) {
		mHeaderText = message;
	}
	
	public interface OnItemSelectedListener {
		public void OnItemSelected(View view, int position, String text);
		public void OnEmptyStateSelected(View view);
	}
	
	public interface onItemChooseListener {
		public void OnItemSelected(View view, Object object);
	}
	
	private onItemChooseListener mItemChooseListener;
	public void setItemChooseListener(onItemChooseListener l) {
		mItemChooseListener = l;
	}
}
