package au.com.alphsigma.intouch.views;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.ViewAnimator;
import au.com.alphsigma.intouch.R;



public class LonginScreen extends LinearLayout implements OnClickListener {

	public interface LoginScreenListener {
		public void onLogin(String userName, String password);

		public void onRegister(String userName, String password, String age,
				String gender, String zip);
	}

	private LoginScreenListener mLoginScreenListener;

	public void setLoginScreenListener(LoginScreenListener l) {
		mLoginScreenListener = l;
	}

	private static final int LOG_IN_SCREEN = 0;
	private static final int REGISTER_SCREEN = 1;

	ViewAnimator mAnimator;
	View mLoginView;
	View mRegisterView;
	View mTab;
	Button mLoginTab;
	Button mRegisterTab;

	public LonginScreen(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();

		init();

	}

	private void init() {

		mTab = findViewById(R.id.tab_layout);

		AlphaAnimation animation = new AlphaAnimation(0.3f, 1.0f);
		animation.setDuration(1000);
		mAnimator = (ViewAnimator) findViewById(R.id.login_view_animator);
		mAnimator.setInAnimation(animation);
		
		mLoginView = inflate(getContext(), R.layout.login_screen, null);
		mRegisterView = inflate(getContext(), R.layout.register_screen, null);

		mAnimator.addView(mLoginView, LOG_IN_SCREEN);
		mAnimator.addView(mRegisterView, REGISTER_SCREEN);

		mLoginTab = (Button) mTab.findViewById(R.id.tab_login);

		mRegisterTab = (Button) mTab.findViewById(R.id.tab_register);
		mLoginTab.setSelected(true);

		mLoginTab.setOnClickListener(this);
		mRegisterTab.setOnClickListener(this);
		
		mLoginView.findViewById(R.id.login_button).setOnClickListener(this);
		mRegisterView.findViewById(R.id.register_button).setOnClickListener(this);
		

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.tab_login:
			onLoginTabClicked();
			break;
		case R.id.tab_register:
			onRgisterTabClicked();
			break;
		case R.id.login_button:
			onLogin();
			break;
		case R.id.register_button:
			onRegister();
			break;
		}
	}

	private void onLoginTabClicked() {
		mAnimator.setDisplayedChild(LOG_IN_SCREEN);
		mLoginTab.setSelected(true);
		mRegisterTab.setSelected(false);
	}

	private void onRgisterTabClicked() {
		mAnimator.setDisplayedChild(REGISTER_SCREEN);
		mLoginTab.setSelected(false);
		mRegisterTab.setSelected(true);
	}

	private void onLogin() {
		EditText emailFld = (EditText) mLoginView.findViewById(R.id.login_username_fld);
		EditText passwordFld = (EditText) mLoginView.findViewById(R.id.login_password_fld);
		
		if (!TextUtils.isEmpty(emailFld.getText().toString().trim())) {
			if (!TextUtils.isEmpty(passwordFld.getText().toString().trim())) {
				
				String email = emailFld.getText().toString().trim();
				String password = passwordFld.getText().toString().trim();
				
				if (mLoginScreenListener != null)
					mLoginScreenListener.onLogin(email, password);
				
			}else {
				Toast.makeText(getContext(), "Password cannot be empty", Toast.LENGTH_LONG).show();
			}
			
		}else {
			Toast.makeText(getContext(), "Email cannot be empty", Toast.LENGTH_LONG).show();
		}
	}

	private void onRegister() {
		
		EditText emailFld = (EditText) mRegisterView.findViewById(R.id.register_eamil_fld);
		EditText passwordFld = (EditText) mRegisterView.findViewById(R.id.register_password_fld);
		EditText zipFld = (EditText) mRegisterView.findViewById(R.id.register_zipcode_fld);
		Spinner genderSp = (Spinner) mRegisterView.findViewById(R.id.register_gender_fld);
		EditText ageFld = (EditText) mRegisterView.findViewById(R.id.register_age_fld);
		
		
		if (!TextUtils.isEmpty(emailFld.getText().toString().trim())) {
			if (!TextUtils.isEmpty(passwordFld.getText().toString().trim())) {
				
				String email = emailFld.getText().toString().trim();
				String password = passwordFld.getText().toString().trim();
				String zip = zipFld.getText().toString().trim();
				String gender = genderSp.getSelectedItem().toString();
				String age = ageFld.getText().toString().trim();
				
				if (mLoginScreenListener != null) 
					mLoginScreenListener.onRegister(email, password, age, gender, zip);				
				
			}else {
				Toast.makeText(getContext(), "Password cannot be empty", Toast.LENGTH_LONG).show();
			}
			
		}else {
			Toast.makeText(getContext(), "Email cannot be empty", Toast.LENGTH_LONG).show();
		}
		
		
		
	}

}
