package au.com.alphsigma.intouch.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

/**
 * Represents a Page in the ViewPager.
 * It is derived from a layout so that it can contain any type of views in it.
 * @author Ravinder
 *
 */
public class Page extends RelativeLayout{

	public interface PageDataRequestListener {
		public void onDataRequest(Page page, int reqID);
	}
	
	protected PageDataRequestListener mDataRequestListener;
	
	public void setDataRequestListener(PageDataRequestListener l) {
		mDataRequestListener = l;
	}
		
	
	protected String mTitle;
	public Page(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}
	
	public void setTitle(String title) {
		mTitle = title;
	}
	
	public String getTitle() {
		return mTitle;
	}
	
	public void setContent(){
		
	}
		
}
