package au.com.alphsigma.intouch.views;



import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import au.com.alphsigma.intouch.R;

public class PrevReadListAdapter extends BaseAdapter {
	String[] prevread;
	private Context context;
	private LayoutInflater layoutInflater;

	public PrevReadListAdapter(Context c, String[] noti) {
		context = c;
		prevread = noti;
		layoutInflater = LayoutInflater.from(context);
	}

	public int getCount() {
		// TODO Auto-generated method stub
		return prevread.length;
	}

	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@SuppressWarnings("deprecation")
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View grid;
		if (convertView == null) {
			grid = new View(context);
			grid = layoutInflater.inflate(R.layout.prev_read_detail_view, null);

		} else {
			grid = (View) convertView;
		}

		return grid;
	}
}