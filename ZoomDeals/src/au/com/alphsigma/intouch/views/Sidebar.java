package au.com.alphsigma.intouch.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import au.com.alphsigma.intouch.R;



/**
 * Represents the side-bar which slides in and out.
 * @author Ravinder
 *
 */
public class Sidebar extends LinearLayout implements OnClickListener{

	/**
	 * Implement this interface to receive the sidebar item click events
	 * @author Ravinder
	 *
	 */

	Context ctx;
	public interface SidebarItemClickListener {
		public void onSidebarItemClicked(View view);
	}

	public interface ItemClickListener {
		public void onListItemClicked(View view);
	}

	private SidebarItemClickListener mClickListener;
	public void setSidebarItemClickListener(SidebarItemClickListener l) {
		mClickListener = l;
	}

	private RelativeLayout mSidebarView, mSidebarViewRight;

	private SidebarItem mImageView;
	private SidebarItem mProfileView;
	private SidebarItem mLoginView;
	private SidebarItem mLocationView;
	private SidebarItem mFilterView;
	private SidebarItem mVoiceSearchView;
	private RelativeLayout mSidebarFooter;

	private Button mRightPrevReadBtn, mRightPrevReadBtn2;

	public static boolean mIsSidebarAnim = false;

	public Animation slideFromTop, slideFromBottom;
	public String noti[] = {"","","","","","","","","",""};
	public String prevread[] = {"","","","",""};
	public ListView _notiList , _prevReadList;
	public Button _show_prevReadBT , _show_notiReadBT;
	public RelativeLayout	PrevReadList ,	NotiList;
	
	public Sidebar(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		this.ctx = context;
	}


	public void toggleVisibility() {
		if (getVisibility() == VISIBLE) {
			Animation anim = AnimationUtils.loadAnimation(getContext(), R.anim.sidebar_slide_out);
			anim.setAnimationListener(new AnimationListener() {
				@Override
				public void onAnimationEnd(Animation animation) {
					mSidebarView.setVisibility(GONE);
					Sidebar.this.removeAllViews();
					Sidebar.this.setVisibility(GONE);
				}
				@Override
				public void onAnimationRepeat(Animation animation) {
					// TODO Auto-generated method stub

				}
				@Override
				public void onAnimationStart(Animation animation) {
					// TODO Auto-generated method stub

				}
			});

			//mSidebarView.setAnimation(anim);
			mSidebarView.startAnimation(anim);

		} else {
			mIsSidebarAnim = true;
			this.setVisibility(VISIBLE);

			mSidebarView = (RelativeLayout) inflate(getContext(), R.layout.sidebar_layout, null);
			addView(mSidebarView);
			mSidebarView.setVisibility(VISIBLE);

			Animation anim = AnimationUtils.loadAnimation(getContext(), R.anim.sidebar_slide_in);
			mSidebarView.setAnimation(anim);

			updateItems();
		}
	}

	private void updateItems() {

		mImageView = (SidebarItem) mSidebarView.findViewById(R.id.sidebar_image);
		//		mImageView.setTitle(getResources().getString(R.string.filter));
		//		mImageView.setIcon(R.drawable.downloadnew);
		mImageView.setOnClickListener(this);

		mProfileView = (SidebarItem) mSidebarView.findViewById(R.id.sidebar_home);
		mProfileView.setTitle("Home");
		mProfileView.setIcon(R.drawable.home);
		mProfileView.setOnClickListener(this);

		mLoginView = (SidebarItem) mSidebarView.findViewById(R.id.sidebar_chat);
		mLoginView.setTitle("Chats");
		mLoginView.setIcon(R.drawable.chats);
		mLoginView.setOnClickListener(this);

		mLocationView = (SidebarItem) mSidebarView.findViewById(R.id.sidebar_contact);
		mLocationView.setTitle("Contacts");
		mLocationView.setIcon(R.drawable.contacts);
		mLocationView.setOnClickListener(this);

		mFilterView = (SidebarItem) mSidebarView.findViewById(R.id.sidebar_tapit);
		mFilterView.setTitle("Shake To Make");
		mFilterView.setIcon(R.drawable.shaketomake);
		mFilterView.setOnClickListener(this);

		mVoiceSearchView = (SidebarItem) mSidebarView.findViewById(R.id.sidebar_settings);
		mVoiceSearchView.setTitle("Settings");
		mVoiceSearchView.setIcon(R.drawable.settings);
		//mVoiceSearchView.enableToggleBrn();
		mVoiceSearchView.setOnClickListener(this);

		mSidebarFooter = (RelativeLayout) findViewById(R.id.sidebar_footer_holder);
		mSidebarFooter.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {
		if (mClickListener != null) {
			//			if (v.getId() == R.id.sidebar_voice_search) {
			//				mVoiceSearchView.toggleState();
			//			}
			mClickListener.onSidebarItemClicked(v);
		}		
	}

	public void toggleVisibilityRight() {
		if (getVisibility() == VISIBLE) {
			Animation anim = AnimationUtils.loadAnimation(getContext(), R.anim.slide_out_right);
			anim.setAnimationListener(new AnimationListener() {

				@Override
				public void onAnimationEnd(Animation animation) {
					mSidebarViewRight.setVisibility(GONE);
					Sidebar.this.removeAllViews();
					Sidebar.this.setVisibility(GONE);
				}

				@Override
				public void onAnimationRepeat(Animation animation) {
					// TODO Auto-generated method stub
				}

				@Override
				public void onAnimationStart(Animation animation) {
					// TODO Auto-generated method stub
				}
			});
			//mSidebarView.setAnimation(anim);
			mSidebarViewRight.startAnimation(anim);

		} else {
			mIsSidebarAnim = true;
			this.setVisibility(VISIBLE);

			mSidebarViewRight = (RelativeLayout) inflate(getContext(), R.layout.noti_detail, null);
			addView(mSidebarViewRight);
			mSidebarViewRight.setVisibility(VISIBLE);

			Animation anim = AnimationUtils.loadAnimation(getContext(), R.anim.slide_in_left);
			mSidebarViewRight.setAnimation(anim);

			updateRightItems();
		}
	}

	private void updateRightItems() {
		AnimationInitialization();
		NotiList =   (RelativeLayout) findViewById(R.id.noti_layout);

		PrevReadList =   (RelativeLayout) findViewById(R.id.prev_read_layout);


		_show_prevReadBT = (Button) findViewById(R.id.prev_readBT);
		_show_notiReadBT = (Button) findViewById(R.id.prev_readBtn);

		_show_prevReadBT.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				PrevReadList.clearAnimation();
				PrevReadList.startAnimation(slideFromBottom);
				NotiList.setVisibility(View.GONE);
				PrevReadList.setVisibility(View.VISIBLE);
				_prevReadList.setAdapter( new PrevReadListAdapter(ctx, prevread) );
			}
		});
		_show_notiReadBT.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				NotiList.clearAnimation();
				NotiList.startAnimation(slideFromTop);
				NotiList.setVisibility(View.VISIBLE);
				PrevReadList.setVisibility(View.GONE);
				_notiList.setAdapter( new NotiListAdapter(ctx, noti) );
			}
		});

		_prevReadList = (ListView) findViewById(R.id.prev_read_list);
		_notiList = (ListView) findViewById(R.id.noti_list);
		_notiList.setAdapter( new NotiListAdapter(ctx, noti) );

	}
	
	public void AnimationInitialization() {

		// slide from top to bottom ..
		slideFromTop = AnimationUtils.loadAnimation(ctx, R.anim.slide_to_bottom);
		// slide from  bottom to top
		slideFromBottom = AnimationUtils.loadAnimation(ctx, R.anim.slide_to_top);

	}

	public boolean isvisibleLeftBar() {
		// TODO Auto-generated method stub

		if (getVisibility() == VISIBLE) {
			return true;
		}
		return false;
	}

	public boolean isvisibleRightBar() {
		// TODO Auto-generated method stub
		if (getVisibility() == VISIBLE) {
			return true;
		}
		return false;
	}
}
