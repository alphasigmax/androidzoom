package au.com.alphsigma.intouch.webservice;

/**
 * Listener for the requests you make to the Licensing server. 
 * Pass this listener to every request you make and get the callback on the methods.
 * @author Ravinder
 *
 */
public interface ResponseListener {

   public void onComplete(String response, int responseID);
   public void onError(String error, int responseID);

}