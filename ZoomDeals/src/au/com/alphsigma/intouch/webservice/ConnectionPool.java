

package au.com.alphsigma.intouch.webservice;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import au.com.alphsigma.intouch.DataDownloadListener;
import au.com.alphsigma.intouch.ImageDownloadListener;


/**
 * Class to manage the connections, 
 * It creates a looper so the the connection thread is active all the time.
 * @author Ravinder
 *
 */

public class ConnectionPool {

	private static final String TAG = "ConnectionPool";
 
    private final int MSG_GET_DATA = 111;
    private final int MSG_POST = 222;
    private final int MSG_DOWNLOAD_IMAGE = 333;
    private final int MSG_GET_DATA_EX = 444;
    private final int MSG_GET_PLACE = 555;
     
    private static HandlerThread mLooper;
    private static Handler mHandler;
    
    private WebClient mWebClient;
    
    public boolean mIsMock;
    public int mImageType;
    
    public ConnectionPool() {
       if (mLooper == null ) {
    	   mLooper = new HandlerThread("looper");
    	   mLooper.start();
    	   mHandler = new Handler(mLooper.getLooper(), new RequestCallback());
    	}else if (!mLooper.isAlive()) {
    		 mLooper.start();
    	}
       
       mWebClient  = new WebClient();
    }
    
    public ConnectionPool(boolean isMockup) {
    	 if (mLooper == null ) {
      	   mLooper = new HandlerThread("looper_imagedownloader");
      	   mLooper.start();
      	   mHandler = new Handler(mLooper.getLooper(), new RequestCallback());
      	}else if (!mLooper.isAlive()) {
      		 mLooper.start();
      	}
         
         mWebClient  = new WebClient();
    }

    public void getData(Request request, ResponseListener listener) {
    	CustomMessage msg = new CustomMessage();
    	msg.mRequest = request;
    	msg.mCalllback = listener;
    	
    	Message message = Message.obtain();
    	message.obj = msg;
    	//there could be other data messages in the queue, but location is given the high priority
//    	if (request.getRequestId() == LocationActivity.MSG_GET_PLACE) {
//    		
//        	message.what = MSG_GET_PLACE;
//    		mHandler.sendMessageAtFrontOfQueue(message); 
//    	}
//    	else {
//        	message.what = MSG_GET_DATA_EX;
//    		mHandler.sendMessage(message);
//    	}
    }
    
    //TODO: will be removed later
    public void getData(String url, DataDownloadListener callback) {
    	CustomMessage msg = new CustomMessage();
    	msg.mUrl = url;
    	msg.callback = callback;
    	msg.mIsMockup = mIsMock;
    	
    	Message message = Message.obtain();
    	message.obj = msg;
    	message.what = MSG_GET_DATA;
    	
    	mHandler.sendMessage(message);
    }
    
    public void getImage(String url, ImageDownloadListener callback) {
    	CustomMessage msg = new CustomMessage();
    	msg.mUrl = url;
    	msg.callback = callback;
    	msg.mImageType = mImageType;  
    	msg.mIsMockup = mIsMock;
    	
    	Message message = Message.obtain();
    	message.obj = msg;
    	message.what = MSG_DOWNLOAD_IMAGE;
    	
    	mHandler.sendMessage(message);
    }
       
   public void finish() {
	   if (mLooper != null)mLooper.stop();
   }
    
    public void cleanUp() {
    	if (mHandler != null) {
    		mHandler.removeMessages(MSG_GET_DATA);
    		mHandler.removeMessages(MSG_POST);
    		mHandler.removeMessages(MSG_DOWNLOAD_IMAGE);
    	}
    }
   
    private class CustomMessage {
    	Request mRequest;
    	ResponseListener mCalllback;
    	String mUrl;
    	Object callback;
    	boolean mIsMockup;
    	int mImageType;
    }
    
    private class RequestCallback implements Handler.Callback {
		public boolean handleMessage(Message msg) {
			
			switch(msg.what) {
				case MSG_GET_DATA: 
					if (mWebClient != null) {
						CustomMessage data = (CustomMessage) msg.obj;
						mWebClient.mIsMockup = data.mIsMockup;
						mWebClient.download(data.mUrl, (DataDownloadListener)data.callback);
					}
				     break;
				case MSG_GET_DATA_EX:
					
					if (mWebClient != null) {
						CustomMessage data = (CustomMessage) msg.obj;
						mWebClient.mIsMockup = data.mIsMockup;
						mWebClient.request(data.mRequest, data.mCalllback);
					}
					break;
				case MSG_GET_PLACE:
					if (mWebClient != null) {
						CustomMessage data = (CustomMessage) msg.obj;
						mWebClient.mIsMockup = data.mIsMockup;
						mWebClient.requestEx(data.mRequest, data.mCalllback);
					}
					break;
				case MSG_POST:
				     break;
				case MSG_DOWNLOAD_IMAGE:
					if (mWebClient != null) {
						CustomMessage data = (CustomMessage) msg.obj;
						mWebClient.mIsMockup = data.mIsMockup;
						mWebClient.mImageType = data.mImageType;
						
						mWebClient.download(data.mUrl, 
								(ImageDownloadListener)data.callback);
					}
					break;
	          default: break;
	               
			}
			
			return false;
		}
    	
    }

}
