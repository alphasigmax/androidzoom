package au.com.alphsigma.intouch.webservice;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;

/**
 * Class to make http requests. Each request 
 * @author Ravinder
 *
 */
public class Request {
	
	protected String mBaseURL = "";
	private HttpPost mHttpPostData;
	private HttpGet  mHttpGettData;
	private int mRequestId;
		
	Request(int requestId) {
		mRequestId = requestId;
		mHttpPostData = null;
		mHttpGettData = null;
	}
	
	public void setHttpGetMethod(HttpGet httpGet) {
		mHttpGettData = httpGet;
	}
	
	public void setHttpPostMethod(HttpPost httpPost) {
		mHttpPostData = httpPost;
	}
	
	public HttpUriRequest getHttpData() {
		HttpUriRequest httpObject = null;
		if (mHttpGettData != null) {
			httpObject = mHttpGettData;
		} else if ( mHttpPostData != null) {
			httpObject = mHttpPostData;
		}
		return httpObject;
	}
	
	public int getRequestId() {
		return mRequestId;
	}
}
