package au.com.alphsigma.intouch.webservice;


public class Response {
	private String mResponse;
	private int mRequestID;
	
	
	public Response(String response, int responseId) {
		mRequestID = responseId;
	}
	
	
	public int getRequestId() {
		return mRequestID;
	}
	
	
	
}
