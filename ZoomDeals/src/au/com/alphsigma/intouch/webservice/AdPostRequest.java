package au.com.alphsigma.intouch.webservice;


/**
 * Ad Post request object. The required parameters if any are added to the base url here
 * @author Ravinder
 *
 */
public class AdPostRequest extends Request{

	public AdPostRequest(int requestId) {
		super(requestId);
		
//		mBaseURL = WebUrls.ZOOMDEALS_ADPOST_URL;
//		HttpGet httpGet = new HttpGet(mBaseURL);
//		setHttpGetMethod(httpGet);
	}

}
