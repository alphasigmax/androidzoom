package au.com.alphsigma.intouch.webservice;

import org.apache.http.client.methods.HttpGet;

public class PlacesRequest extends Request {

	public PlacesRequest(int requestId, String url) {
		super(requestId);
		mBaseURL = url;
		HttpGet httpGet = new HttpGet(mBaseURL);
		setHttpGetMethod(httpGet);
		
	}

}
