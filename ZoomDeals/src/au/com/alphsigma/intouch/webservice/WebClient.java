package au.com.alphsigma.intouch.webservice;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import au.com.alphsigma.intouch.DataDownloadListener;
import au.com.alphsigma.intouch.ImageDownloadListener;


/**
 * Class to make HTTP connections and read data from the web server.
 * @author Ravinder
 *
 */

public class WebClient {

	private static final String TAG = "WebClient";
	
	public boolean mIsMockup = false;
	public int mImageType;
	
	public WebClient() {
		
	}
	public WebClient(boolean ismockup) {
		mIsMockup = ismockup;
	}
	
	
	public boolean request(final Request request, ResponseListener listener) {
		boolean bRet = false;
		HttpResponse httpResponse;
			HttpClient httpclient= new DefaultHttpClient();
			try {
				Log.d(TAG, "URL: " + request.getHttpData().getURI().toString());
				httpResponse =  httpclient.execute(request.getHttpData());
				int statusCode = httpResponse.getStatusLine().getStatusCode();
				if (statusCode == 200) { //if HTTP_OK
					String responseStr = readResponse(httpResponse);
					//response.setResposeString(responseStr);  
					if (listener != null) listener.onComplete(responseStr, request.getRequestId());
					bRet = true;
				} else {
					if (listener != null) listener.onError(statusCode+" " +httpResponse.getStatusLine().getReasonPhrase(),
							request.getRequestId());
				}


			} catch (ClientProtocolException e) {
				e.printStackTrace();
				if (listener != null) listener.onError(e.toString(), request.getRequestId());

			} catch (IOException e) {
				e.printStackTrace();
				if (listener != null) listener.onError(e.toString(), request.getRequestId());
			}
		return bRet;
	}
	
	public void requestEx(final Request request, ResponseListener listener) {
		HttpURLConnection conn = null;
	    StringBuilder jsonResults = new StringBuilder();
	    try {
	       	        
	        URL url = new URL(request.getHttpData().getURI().toString());
	        conn = (HttpURLConnection) url.openConnection();
	        InputStreamReader in = new InputStreamReader(conn.getInputStream());
	        
	        // Load the results into a StringBuilder
	        int read;
	        char[] buff = new char[1024];
	        while ((read = in.read(buff)) != -1) {
	            jsonResults.append(buff, 0, read);
	        }
	        if (listener != null) listener.onComplete(jsonResults.toString(), request.getRequestId());
	        
	    } catch (IOException e) {
	    } finally {
	        if (conn != null) {
	            conn.disconnect();
	        }
	    }
	}
	
	private String readResponse(HttpResponse response) {
		Log.d(TAG, "===============Zoomdeals Response==================");
   		StringBuilder sb = new StringBuilder();
   		try {
   			InputStream in = response.getEntity().getContent();
   		
   			BufferedReader r = new BufferedReader(new InputStreamReader(in), 1000);
   			int i  = 0;
   			for (String line = r.readLine(); line != null; line = r.readLine()) {
   				sb.append(line);
   			}
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
   		Log.d(TAG, sb.toString());
   		Log.d(TAG, "===============Zoomdeals Response==================");
        
        return sb.toString();
   	}
	
	public void download(String url, DataDownloadListener listener) {
		if (!mIsMockup) {
				
		}else { //for mock-up
			if (listener != null) {
				//listener.onComplete(mMockup_Json);
			}
		}
	}
	
	public void download(String url, ImageDownloadListener callback) {
		if (mIsMockup) {
			
			if (mImageType == 1) {
				//callback.onComplete(getLOGOImageRes());
			}else {
				//callback.onComplete(getBGImageRes());
			}
		}else {
			
			URL downloadUrl = null;
			InputStream inStream = null;
			HttpURLConnection connection;
			Bitmap image = null;
			 
	         try {
	             downloadUrl = new URL(url);
	             connection = (HttpURLConnection) downloadUrl.openConnection();
	
	             Log.d(TAG, "downlaodURL: "+downloadUrl);
	             inStream = connection.getInputStream();
	             
	             Log.d(TAG, (inStream== null)?"ImageStream= null":"ImageStream rcvd");
	             
	             image = BitmapFactory.decodeStream(inStream);
	             
	             callback.onComplete(image);
	             
	         } catch (MalformedURLException e) {
	         	e.printStackTrace();
	         	callback.onError(e.getMessage());
	         } catch (IOException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
				callback.onError(e2.getMessage());
			}
	         finally {
	        	if (inStream != null)
					try {
						inStream.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	         }
			
		}
	}
	
	
}
