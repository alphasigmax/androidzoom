package au.com.alphsigma.intouch;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;
import au.com.alphsigma.intouch.views.Titlebar;
import au.com.alphsigma.intouch.views.Titlebar.TitlebarItemClickListener;

public class FriendRequestActivity extends Activity{
	private Button acceptBtn, rejectBtn; 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.friend_request);
		
		acceptBtn = (Button) findViewById(R.id.friend_req_accpt_btn);
		rejectBtn = (Button) findViewById(R.id.friend_req_reject_btn);
		
		Titlebar titlebar = (Titlebar) findViewById(R.id.title_bar);
		titlebar.showOnlyHomeBackIcon();
		titlebar.setTitle(getString(R.string.friend_request));
		
		titlebar.setTitlebarITemClickListener(new TitlebarItemClickListener() {
			@Override
			public void onItemClicked(View view) {
				finish();
			}
		});
		
		acceptBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(FriendRequestActivity.this, "Friend Request Accepted.. ", Toast.LENGTH_LONG).show();
				finish();
			}
		});
		
		rejectBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Toast.makeText(FriendRequestActivity.this, "Friend Request Rejected.. ", Toast.LENGTH_LONG).show();
				finish();
			}
		});
		
		
	}

}
