package au.com.alphsigma.intouch;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import au.com.alphsigma.intouch.views.Titlebar;
import au.com.alphsigma.intouch.views.Titlebar.TitlebarItemClickListener;


public class LoginActity extends Activity {
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.login_layout);
		
		Titlebar titlebar = (Titlebar) findViewById(R.id.title_bar);
		titlebar.showOnlyHomeIcon();
		titlebar.setTitle(getString(R.string.login));
		
		titlebar.setTitlebarITemClickListener(new TitlebarItemClickListener() {
			
			@Override
			public void onItemClicked(View view) {
				finish();
				
			}
		});
		       
	}
}
