package au.com.alphsigma.intouch.model;

import java.util.List;


public class Post implements Comparable<Post> {

    private String userName;
	private String userType;
	private String userImg;
	private String postType;
	private String userPost;
	private String postImg;
	private int mediaType;
	public int getMediaType() {
		return mediaType;
	}

	public void setMediaType(int mediaType) {
		this.mediaType = mediaType;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getUserImg() {
		return userImg;
	}

	public void setUserImg(String userImg) {
		this.userImg = userImg;
	}

	public String getPostType() {
		return postType;
	}

	public void setPostType(String postType) {
		this.postType = postType;
	}

	public String getUserPost() {
		return userPost;
	}

	public void setUserPost(String userPost) {
		this.userPost = userPost;
	}

	public String getPostImg() {
		return postImg;
	}

	public void setPostImg(String postImg) {
		this.postImg = postImg;
	}

	public String getPostVid() {
		return postVid;
	}

	public void setPostVid(String postVid) {
		this.postVid = postVid;
	}

	public String getPostPlus() {
		return postPlus;
	}

	public void setPostPlus(String postPlus) {
		this.postPlus = postPlus;
	}

	public String getPostCommentCount() {
		return postCommentCount;
	}

	public void setPostCommentCount(String postCommentCount) {
		this.postCommentCount = postCommentCount;
	}

	public List<Comments> getComments() {
		return comments;
	}

	public void setComments(List<Comments> comments) {
		this.comments = comments;
	}

	private String postVid;
	private String postPlus;
	private String postCommentCount;
    private List<Comments> comments;

    public Post() {

    }

    public Post(String name, String type, String userImg, String postType, String post, String postImg,String postVid,String postPlus, String postComments,int mediaType, List<Comments> comments) {
        this.userName 	= name;
        this.userType 	= type;
        this.userImg 	= userImg;
        this.postType 	= postType;
        this.userPost 	= post;
        this.postImg 	= postImg;
        this.postVid 	= postVid;
        this.postPlus	= postPlus;
        this.postCommentCount = postComments;
        this.comments 	= comments;
        this.mediaType  = mediaType;
    }

	@Override
	public int compareTo(Post arg0) {
		// TODO Auto-generated method stub
		return 0;
	}
  
}
