package au.com.alphsigma.intouch.model;


public class Comments {

    private String _name;
    private String _img;
    private String _comment;

    public Comments() {

    }

    public Comments(String name, String img, String comment) {
        this._name 		= name;
        this._img 		= img;
        this._comment 	= comment;
    }

    public String get_img() {
		return _img;
	}

	public void set_img(String _img) {
		this._img = _img;
	}

	public String get_comment() {
		return _comment;
	}

	public void set_comment(String _comment) {
		this._comment = _comment;
	}

	public String getName() {
        return this._name;
    }
    public void setName(String name) {
        this._name = name;
    }
}
