package au.com.alphsigma.intouch.model;

import android.net.Uri;
import au.com.alphsigma.intouch.webservice.ConnectionPool;


public class Chat {

	/**
	 * Listener to notify that the object has downloaded something and the UI needs to be refreshed
	 * @author shamanth
	 *
	 */
	public interface ListRefreshListener{
		public void refresh(Chat deal);
	}
	private ListRefreshListener mListener;
	public void setRefreshListener(ListRefreshListener l) {
		mListener = l;
	}
	public int mChatID;
	public String mDateCreated;
	public String mChatText;
	public boolean mIsRead;
	public String mFrom;
	public String mTo;
	public int mCount;
	public Uri mImageUri;
	public boolean mIsFavorite;
	public boolean mIsOnline;
	
	public static ConnectionPool mConnectionPool;
	
    
    //The listview calls refresh so many times, If the images are downloaded then no need to download again
    private boolean mIsImageDownloaded = false;
    
    //The listview calls refresh so many times, but we should not make download requests those many times
    private boolean mIsPendingRequestsForImg = false;
	
	private Uri mImageCacheUri;
	
	
	public Chat() {
		if (mConnectionPool == null) 
			mConnectionPool = new ConnectionPool(true);
	}
	
//	public void fetchLogo() {
//		if (mConnectionPool != null && !mIsImageDownloaded 
//									&& !mIsPendingRequestsForImg) {
//			mConnectionPool.mIsMock = false;
//			mConnectionPool.mImageType = 1;
//			String url = String.format(WebUrls.ZOOMDEALS_ADPOST_LOGO_URL, mMerchantID);
//			mConnectionPool.getImage(url, new ImageDownloadListener() {
//				
//				@Override
//				public void onComplete(Bitmap bmp) {
//					if (bmp != null) {
//						
//						String path = cacheLOGOImage(bmp);
//						if (!TextUtils.isEmpty(path))
//							mLogoImageCacheUri = Uri.fromFile(new File(path));
//						//mMerchantLogoBmp = bmp.copy(bmp.getConfig(), true);
//						
//						bmp.recycle();
//						bmp = null;
//						
//						mIsLogoImageDownloaded = true;
//						mIsPendingRequestsForLogo = true;
//						
//						if (mListener != null) mListener.refresh(Chat.this);
//						
//						
//						
//					}
//				}
//
//				@Override
//				public void onError(String msg) {
//					mIsLogoImageDownloaded = true;
//					mIsPendingRequestsForLogo = false;
//				}
//			});
//			
//			mIsPendingRequestsForLogo = true;
//			
//		}
//	}
	
	
	
	
//	public Uri getLogoImageUri() {
//		return mLogoImageCacheUri;
//	}
//		
//
//	
//	private String cacheLOGOImage(Bitmap logoBitmap) {
//		String cacheDir = Launcher.CACAHE_DIR;//Utils.createCahcheDir();
//		if (!TextUtils.isEmpty(cacheDir)) {
//			String fileName = cacheDir + "/"+ mDealID + "merchant.jpg";
//			boolean isOk = Utils.saveToFile(logoBitmap, fileName);
//			return (isOk)?fileName:null;
//		}
//		return null;
//	}
//	
//	
//	
//	public boolean isLogoDownloaded() {
//		return mIsLogoImageDownloaded;
//	}

	
}
