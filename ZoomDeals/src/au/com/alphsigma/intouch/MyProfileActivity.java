package au.com.alphsigma.intouch;

import java.util.ArrayList;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import au.com.alphsigma.intouch.model.Post;
import au.com.alphsigma.intouch.views.AboutPage;
import au.com.alphsigma.intouch.views.HomePage;
import au.com.alphsigma.intouch.views.Page;
import au.com.alphsigma.intouch.views.PhotosPage;
import au.com.alphsigma.intouch.views.Titlebar;
import au.com.alphsigma.intouch.views.Titlebar.TitlebarItemClickListener;

import com.viewpagerindicator.TitlePageIndicator;

public class MyProfileActivity extends Activity{
	private ViewPager mViewPager;
	private PageAdapter mPageAdapter;
	private ArrayList<Page> mPages = new ArrayList<Page>();
	private TitlePageIndicator mPageTitleIndicator;

	OnPageChangeListener mPageChangeListener;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.profile_layout);

		Titlebar titlebar = (Titlebar) findViewById(R.id.title_bar);
		titlebar.showOnlyHomeIcon();
		titlebar.setTitle(getString(R.string.profile));
		titlebar.setTitlebarITemClickListener(new TitlebarItemClickListener() {
			@Override
			public void onItemClicked(View view) {
				finish();
			}
		});

		ImageView imgWall		    = (ImageView) findViewById(R.id.profile_wall_pic);
		ImageView imgProfile	    = (ImageView) findViewById(R.id.profile_pic);
		TextView  txtName		    = (TextView)  findViewById(R.id.profile_name);
		TextView  txtWorksAt1	    = (TextView)  findViewById(R.id.profile_works_at1);
		//	TextView  txtWorksAt2	    = (TextView)  findViewById(R.id.profile_works_at2);
		//	TextView  txtWorksAt3	    = (TextView)  findViewById(R.id.profile_works_at3);
		//  TextView  txtFollowerCount	= (TextView)  findViewById(R.id.profile_follower_count);


		Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ic_nav_profile);
		imgProfile.setImageBitmap(Utils.getCircleBitmap(bitmap, 10));
		if(getIntent() != null && getIntent().hasExtra("index")){
			Post post = HomePage.objHomePage.postList.get(getIntent().getIntExtra("index", 0));
			txtName.setText(post.getUserName());
		}else{
			txtName.setText("Felix");
		}
		//txtWorksAt1.setText("Works at Australia Post");
		//txtWorksAt2.setText("Attended Fergusson");
		//txtWorksAt3.setText("Currently in Docklands VIC, Australia");
		//txtFollowerCount.setText("114 followers");
		loadView();

	}
	private void loadView() {

		constructPages();
		mViewPager 			= (ViewPager) findViewById(R.id.pager);
		mPageAdapter 		= new PageAdapter();
		mViewPager.setAdapter(mPageAdapter);

		mPageTitleIndicator = (TitlePageIndicator)findViewById(R.id.indicator);
		mPageTitleIndicator.setViewPager(mViewPager);
		mPageTitleIndicator.setCurrentItem(1);
		mPageChangeListener = new OnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {

			}
			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {				

			}
			@Override
			public void onPageScrollStateChanged(int arg0) {
				//ListPage.mStopRefreshing = true;
			}
		};

		mPageTitleIndicator.setOnPageChangeListener(mPageChangeListener);
	}

	/**
	 * Create all Page objects and add them to the Pages list.
	 */
	private void constructPages() {
		AboutPage aboutPage = new AboutPage(this, null);
		//aboutPage.setContent(); 
		aboutPage.setTitle("About");
		mPages.add(aboutPage);

		//		ChatsPage chatPage = new ChatsPage(this, null);
		//		//chatPage.setContent(); 
		//		chatPage.setTitle("Chats");
		//		mPages.add(chatPage);

		HomePage postPage = new HomePage(this, null);
		//postPage.setContent(); 
		postPage.setTitle("Posts");
		mPages.add(postPage);

		PhotosPage photosPage = new PhotosPage(this, null);
		//photosPage.setContent();
		photosPage.setTitle("Photos");
		mPages.add(photosPage);

	}
	/**
	 * Page adapter for the Pages.
	 * Object included from the android.support.v4.view.PagerAdapter 
	 * @author Ravinder
	 *
	 */
	private class PageAdapter extends PagerAdapter {

		private int MAX_PAGES = 3;

		@Override
		public int getCount() {
			return MAX_PAGES;
		}

		@Override
		public Object instantiateItem(View container, int position) {
			Page page = null;
			if (position >= 0 && position < mPages.size()){
				page = mPages.get(position);
				page.setContent();
				((ViewPager) container).addView(page);
			}	
			return page;
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view==((Page)object);
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object view) {
			((ViewPager) container).removeView((Page)view);
		}

		@Override
		public CharSequence getPageTitle(int position) {
			if (position >= 0 && position < mPages.size())
				return mPages.get(position).getTitle();
			else return "";
		}  	
	}
}
