package au.com.alphsigma.intouch;

import java.util.ArrayList;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import au.com.alphsigma.intouch.model.Chat;
import au.com.alphsigma.intouch.model.Chat.ListRefreshListener;
import au.com.alphsigma.intouch.views.ChatsPage;
import au.com.alphsigma.intouch.views.ContactPage;
import au.com.alphsigma.intouch.views.HomePage;
import au.com.alphsigma.intouch.views.Page;
import au.com.alphsigma.intouch.views.Sidebar;
import au.com.alphsigma.intouch.views.Sidebar.SidebarItemClickListener;
import au.com.alphsigma.intouch.views.Titlebar;
import au.com.alphsigma.intouch.views.Titlebar.TitlebarItemClickListener;
import au.com.alphsigma.intouch.webservice.ConnectionPool;
import au.com.alphsigma.intouch.webservice.ResponseListener;

import com.viewpagerindicator.TitlePageIndicator;

/**
 * This is the main activity for the application.
 * 
 * @author Ravinder
 * 
 */
public class Launcher extends Activity implements TitlebarItemClickListener,
		SidebarItemClickListener, ResponseListener {

	public static final String PREFS_FILE = "zoomdeals";

	private ViewPager mViewPager;
	private PageAdapter mPageAdapter;
	private Titlebar mTitleBarView;
	private Sidebar mSidebar, mSidebarRight;

	private ArrayList<Page> mPages = new ArrayList<Page>();
	private TitlePageIndicator mPageTitleIndicator;
	private ConnectionPool mConnectionPool;

	OnPageChangeListener mPageChangeListener;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mConnectionPool = new ConnectionPool();
		mConnectionPool.mIsMock = true; // to be removed later

		loadView();
		 
	}

	@Override
	protected void onResume() {
		if (mViewPager != null) {
			// int item = mViewPager.getCurrentItem();
			// if(item == 1) {
			// if (mPages.size() > 1 && ((ListPage)mPages.get(1)) != null) {
			// ((ListPage)mPages.get(1)).refreshList();
			// }
			// }
		}
		super.onResume();
	}

	@Override
	protected void onPause() {
		try {

		} catch (Exception e) {
			e.printStackTrace();
		}
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// pauseMixare();
		try {

		} catch (Exception e) {
			e.printStackTrace();
		}
		super.onDestroy();
	}

	@Override
	public void finish() {
		try {

		} catch (Exception e) {
			e.printStackTrace();
		}
		if (mConnectionPool != null) {
			mConnectionPool.cleanUp();
			// mConnectionPool.finish();
		}
		super.finish();
	}

	private void loadView() {

		setContentView(R.layout.main);

		constructPages();

		mViewPager = (ViewPager) findViewById(R.id.pager);
		mPageAdapter = new PageAdapter();
		mViewPager.setAdapter(mPageAdapter);

		mPageTitleIndicator = (TitlePageIndicator) findViewById(R.id.indicator);
		mPageTitleIndicator.setViewPager(mViewPager);
		mPageTitleIndicator.setCurrentItem(0);

		mTitleBarView = (Titlebar) findViewById(R.id.title_bar);
		mTitleBarView.setTitlebarITemClickListener(Launcher.this);
		mTitleBarView.showOnlyHomeIcon();

		mSidebar = (Sidebar) findViewById(R.id.sidebar_holder);
		mSidebar.setSidebarItemClickListener(Launcher.this);

		mSidebarRight = (Sidebar) findViewById(R.id.sidebar_holder_right);
		mSidebarRight.setSidebarItemClickListener(Launcher.this);

		mPageChangeListener = new OnPageChangeListener() {
			int positionCurrent;
			boolean dontLoadList;

			@Override
			public void onPageSelected(int position) {

			}

			@Override
			public void onPageScrolled(int position, float positionOffset,
					int positionOffsetPixels) {
				// positionCurrent = position;
				// if( positionOffset == 0 && positionOffsetPixels == 0 ){ //
				// the offset is zero when the swiping ends{
				// dontLoadList = false;
				// }
				// else{
				// dontLoadList = true; // To avoid loading content for list
				// after swiping the pager.
				// }
			}

			@Override
			public void onPageScrollStateChanged(int state) {
				// ListPage.mStopRefreshing = true;
				// if(state == 0){ // the viewpager is idle as swipping ended
				// new Handler().postDelayed(new Runnable() {
				// public void run() {
				// if(!dontLoadList){
				// //async thread code to execute loading the list...
				// }
				// }
				// },200);
				// }
			}
		};

		mPageTitleIndicator.setOnPageChangeListener(mPageChangeListener);
	}

	/**
	 * Create all Page objects and add them to the Pages list.
	 */
	private void constructPages() {
		HomePage homePage = new HomePage(this, null);
		// homePage.setContent();
		homePage.setTitle("Home");
		mPages.add(homePage);
		ChatsPage chatPage = new ChatsPage(this, null);
		// chatPage.setContent();
		chatPage.setTitle("Chats");
		mPages.add(chatPage);
		ContactPage contactsPage = new ContactPage(this, null);
		// contactsPage.setContent();
		contactsPage.setTitle("Contacts");
		mPages.add(contactsPage);

	}

	


	/**
	 * Callback received on title bar item click
	 */
	@Override
	public void onItemClicked(View view) {
		switch (view.getId()) {
		case R.id.title_home_btn:
			// ListPage.mStopRefreshing = true;
			// mSidebar.toggleVisibility();

			if (mSidebar.isvisibleLeftBar() == true) {
				mSidebar.toggleVisibility();
			} else if (mSidebarRight.isvisibleRightBar() == true) {
				mSidebarRight.toggleVisibilityRight();
				mSidebar.toggleVisibility();
			} else {
				mSidebar.toggleVisibility();
			}

			break;
		// Toast.makeText(Launcher.this, "Home", Toast.LENGTH_SHORT).show();
		// break;
		case R.id.title_menu_btn:
			// Toast.makeText(Launcher.this, "Menu", Toast.LENGTH_SHORT).show();
			// launchActivity(new Intent(Launcher.this, NotificationsActivity.class));
			// overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
			// mSidebarRight.toggleVisibilityRight();

			if (mSidebarRight.isvisibleRightBar() == true) {
				mSidebarRight.toggleVisibilityRight();
			} else if (mSidebar.isvisibleLeftBar() == true) {
				mSidebar.toggleVisibility();
				mSidebarRight.toggleVisibilityRight();
			} else {
				mSidebarRight.toggleVisibilityRight();
			}
			
			

			break;

		// case R.id.title_whats_hot_btn:
		// Toast.makeText(Launcher.this, "Whats Hot?",
		// Toast.LENGTH_SHORT).show(); break;
		// case R.id.title_whats_new_btn:
		// Toast.makeText(Launcher.this, "Whats New?",
		// Toast.LENGTH_SHORT).show(); break;
		// case R.id.title_refresh_btn:
		// Toast.makeText(Launcher.this, "Refresh", Toast.LENGTH_SHORT).show();
		// break;
		// case R.id.title_deals_for_me:
		// Toast.makeText(Launcher.this, "Deals for me",
		// Toast.LENGTH_SHORT).show(); break;
		default:
			break;
		}
	}
	Animation slideinTop, slideinBottom;
	@Override
	public void onSidebarItemClicked(View view) {
		// mSidebar.toggleVisibility();
 
		int height = 0;
		switch (view.getId()) {
		case R.id.sidebar_image:

			launchActivity(new Intent(Launcher.this, MyProfileActivity.class));
			overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
			break;
		case R.id.sidebar_home:
			mSidebar.toggleVisibility();
			mPageTitleIndicator.setCurrentItem(0);
			break;
		case R.id.sidebar_chat:
			mSidebar.toggleVisibility();
			mPageTitleIndicator.setCurrentItem(1);
			break;
		case R.id.sidebar_contact:
			mSidebar.toggleVisibility();
			mPageTitleIndicator.setCurrentItem(2);
			break;
		case R.id.sidebar_tapit:
			mSidebar.toggleVisibility();
			launchActivity(new Intent(Launcher.this, FilterActivity.class));
			overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
			break;
		case R.id.sidebar_settings:
			mSidebar.toggleVisibility();
			// launchVoiceRecognizer();
			break;
		// Toast.makeText(Launcher.this, "Voice Search",
		// Toast.LENGTH_SHORT).show(); break;
		// case R.id.sidebar_image_search:
		// Toast.makeText(Launcher.this, "Image Search",
		// Toast.LENGTH_SHORT).show(); break;
		// case R.id.sidebar_notify_mgr:
		// launchActivity(new Intent(Launcher.this,
		// NotificationMgrActivity.class)); break;
		// case R.id.sidebar_help:
		// //launchActivity(new Intent(Launcher.this, HelpActivity.class));
		// break;
		// Toast.makeText(Launcher.this, Utils.getUID(this),
		// Toast.LENGTH_LONG).show(); break;
		case R.id.sidebar_footer_holder:
			mSidebar.toggleVisibility();
			launchActivity(new Intent(Launcher.this, NotificationsActivity.class));
			break;
		 
			
		default:
			break;
		}
	}

	private void launchActivity(Intent intent) {
		try {
			startActivity(intent);
		} catch (ActivityNotFoundException e) {

		}
	}

	private ListRefreshListener mRefreshListener = new ListRefreshListener() {

		@Override
		public void refresh(final Chat chat) {
			Launcher.this.runOnUiThread(new Runnable() {

				@Override
				public void run() {
					((ChatsPage) mPages.get(1)).refreshList(chat);

				}
			});
		}
	};

	/**
	 * Page adapter for the Pages. Object included from the
	 * android.support.v4.view.PagerAdapter
	 * 
	 * @author Ravinder
	 * 
	 */
	private class PageAdapter extends PagerAdapter {

		private int MAX_PAGES = 3;

		@Override
		public int getCount() {
			return MAX_PAGES;
		}

		@Override
		public Object instantiateItem(View container, int position) {
			Page page = null;
			if (position >= 0 && position < mPages.size()) {
				Log.v("PAGE", "" + position);
				page = mPages.get(position);
				page.setContent();
				((ViewPager) container).addView(page);
			}

			return page;
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view == ((Page) object);
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object view) {
			((ViewPager) container).removeView((Page) view);
		}

		@Override
		public CharSequence getPageTitle(int position) {
			if (position >= 0 && position < mPages.size())
				return mPages.get(position).getTitle();
			else
				return "";
		}
	}

	public void onConfigurationChanged(Configuration newConfig) {

		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onComplete(String response, int responseID) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onError(String error, int responseID) {
		// TODO Auto-generated method stub

	}

}