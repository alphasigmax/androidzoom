package au.com.alphsigma.intouch;

public interface DataDownloadListener {
	public void onComplete(String response);
	public void onError(String errorMsg);
}
